/**
 * @file
 * Js to validate and configure mobile using miniorange app.
 */

(function($) {
    Drupal.behaviors.configure_phone = {
        attach : function(context, settings) {
            var timeout;
            // Qr code verification.
            if ($('#displayQrCode').hasClass('qr_configure')) {
                poll_mobile_registration();
            }
            if ($('#displayQrCode').hasClass('qr_validate')) {
                poll_mobile_validation();
            }
            // Email verification.
            if ($('.email-wrap').hasClass('email-verification')) {
                poll_email_validation();
            }

            // Accordion effect in troubleshooting page.
            $('.trb_collapse').hide();
            $('.mo2f_table_layout h3').click(
                function(){
                    if ($(this).hasClass('expand')) {
                        $(this).removeClass('expand').next('.trb_collapse').slideUp();
                    }
                    else {
                        $(this).addClass('expand').next('.trb_collapse').slideDown();
                    }
                }
            );

            // On selection of auth type submits the form.
            $("#two-factor-authentication-form input[type='radio']").click(function(){
              $("#two-factor-authentication-form").submit();
            });
            // On selection of phone type submits the form
            $("#google-authenticator input[type='radio']").click(function(){
              $("#google-authenticator").submit();
            });

            // Phone country code.
            $("#edit-country-code option").filter(
                function() {
                    return $(this).val() == $(".form-item-phone #edit-phone").val();
                }
            ).attr('selected', true);
            $("#edit-country-code").change(
                function() {

                    $(".form-item-phone #edit-phone").val($(this).find("option:selected").attr("value"));
                }
            );
            $(".form-item-phone #edit-phone").val($('#edit-country-code').find("option:selected").attr("value"));
            // Phone country code for support block
            $("#edit-country-code--2 option").filter(
                function() {
                    return $(this).val() == $("#tfa-user-contact-us .form-item-phone #edit-phone--2").val();
                }
            ).attr('selected', true);
            $("#edit-country-code--2").change(
                function() {

                    $("#tfa-user-contact-us .form-item-phone #edit-phone--2").val($(this).find("option:selected").attr("value"));
                }
            );
            $("#tfa-user-contact-us .form-item-phone #edit-phone--2").val($('#edit-country-code--2').find("option:selected").attr("value"));

            // Disabled button of login settings page
            // $('#tfa-user-settings input[type="submit"]').attr('disabled','disabled');.
        }

    };

    function poll_mobile_registration() {
        var transId = Drupal.settings.two_factor_authentication.mo2f_qrCode;
        var jsonString = "{\"txId\":\"" + transId + "\"}";
        var postUrl = Drupal.settings.two_factor_authentication.url
        + "/moas/api/auth/registration-status";

        jQuery
        .ajax(
            {
                url : postUrl,
                type : "POST",
                dataType : "json",
                data : jsonString,
                contentType : "application/json; charset=utf-8",
                success : function(result) {
                    var status = JSON.parse(JSON.stringify(result)).status;
                    if (status == 'SUCCESS') {
                        var content = "<div id='success' style='margin-left: 20px; margin: top:23px;'><img src='"
                        + Drupal.settings.basePath
                        + Drupal.settings.two_factor_authentication.path
                        + "/images/right.png' /></div>";
                        $("#displayQrCode").empty();
                        $("#displayQrCode").append(content);
                        setTimeout(
                            function() {
                                if ($('#edit-qr-scan-submit').length > 0) {
                                    $('#edit-qr-scan-submit').click();
                                }
                            }, 1000
                        );
                    }
                    else if (status == 'ERROR' || status == 'FAILED') {
                        var content = "<div id='error' style='margin-left: 20px; margin: top:23px;'><img src='"
                        + Drupal.settings.basePath
                        + Drupal.settings.two_factor_authentication.path
                        + "/images/wrong.png' /></div>";
                        $("#displayQrCode").empty();
                        $("#displayQrCode").append(content);
                        $('#refrsh_qrcode').show();
                    }
                    else {
                        timeout = setTimeout(poll_mobile_registration, 3000);
                    }
                }
            }
        );
    }

    function poll_mobile_validation() {
        var transId = Drupal.settings.two_factor_authentication.mo2f_qrCode;
        var jsonString = "{\"txId\":\"" + transId + "\"}";
        var postUrl = Drupal.settings.two_factor_authentication.url
        + "/moas/api/auth/auth-status";
        jQuery.ajax(
            {
                url : postUrl,
                type : "POST",
                dataType : "json",
                data : jsonString,
                contentType : "application/json; charset=utf-8",
                success : function(result) {
                    var status = JSON.parse(JSON.stringify(result)).status;
                    console.log(status);
                    if (status == 'SUCCESS') {
                        var content = "<div id='success'><img src='"
                        + Drupal.settings.basePath + Drupal.settings.two_factor_authentication.path
                        + "/images/right.png' /></div>";
                        $("#displayQrCode").empty();
                        $("#displayQrCode").append(content);
                        setTimeout(
                            function() {
                                if ($('#edit-qr-scan-submit').length > 0) {
                                    $('#edit-qr-scan-submit').click();
                                }
                                if ($('#edit-back').length > 0) {
                                    $('#edit-back').click();
                                }
                            }, 1000
                        );
                    }
                    else if (status == 'ERROR' || status == 'FAILED') {
                        var content = "<div id='error'><img src='"
                        + Drupal.settings.basePath + Drupal.settings.two_factor_authentication.path
                        + "/images/wrong.png' /></div>";
                        $("#displayQrCode").empty();
                        $("#displayQrCode").append(content);
                        setTimeout(
                            function() {
                                                }, 1000
                        );
                    }
                    else {
                        timeout = setTimeout(poll_mobile_validation, 3000);
                    }
                }
            }
        );
    }

    /*var timeout;
    poll_email_validation();*/
    function poll_email_validation()
    {
        var transId = Drupal.settings.two_factor_authentication.mo2f_qrCode;
        console.log(transId);
        var jsonString = "{\"txId\":\"" + transId + "\"}";
        var postUrl = Drupal.settings.two_factor_authentication.url
        + "/moas/api/auth/auth-status";
        console.log(postUrl);

        jQuery.ajax(
            {
                url: postUrl,
                type : "POST",
                dataType : "json",
                data : jsonString,
                contentType : "application/json; charset=utf-8",
                success : function(result) {
                    var status = JSON.parse(JSON.stringify(result)).status;
                    console.log(status);
                    if (status == 'SUCCESS') {
                        console.log($('#edit-qr-scan-submit').length);
                        /*setTimeout(function() {
                        }, 1000);*/
                        setTimeout(
                            function() {
                                if ($('#edit-test-email-verify-submit').length > 0) {
                                    $('#edit-test-email-verify-submit').click();
                                }
                                if ($('#edit-qr-scan-submit').length > 0) {
                                    console.log('yes');
                                    $('#edit-qr-scan-submit').click();
                                }
                            }, 1000
                        );
                    }
                    else if (status == 'ERROR' || status == 'FAILED' || status == 'DENIED') {
                        if ($('#edit-email-login-failed').length > 0) {
                            $('#edit-email-login-failed').click();
                        }
                    }
                    else {
                        timeout = setTimeout(poll_email_validation, 3000);
                    }
                }
            }
        );
    }
    $(document).ready(function() {
jQuery('#myCarousel').carousel('pause');
      jQuery('#helpLink').click(function(e) {

        jQuery('#showQRHelp').show();
        jQuery('.qr_validate').hide();
        jQuery('#myCarousel').carousel(0);
        e.preventDefault();
      });
      jQuery('#qrLink').click(function(e) {
        jQuery('#showQRHelp').hide();
        jQuery('.qr_validate').show();
        jQuery('#myCarousel').carousel('pause');
        e.preventDefault();
      });

});
$(document).ready(function() {

        jQuery('#otpHelpLink').click(function(e) {
        jQuery('#showOTPHelp').show();
        jQuery('.showOTP').hide();
        jQuery('#otpHelpLink').hide();
        e.preventDefault();
      });
      jQuery('#otpLink').click(function(e) {
        jQuery('#showOTPHelp').hide();
        jQuery('.showOTP').show();
        jQuery('#otpHelpLink').show();
        e.preventDefault();
      });
      jQuery('#disptext').click(function(e) {
          jQuery('.scanqr').toggleClass('closed');
          e.preventDefault();
      });
      jQuery('#google-authenticator .step-3 #edit-ga-otp').keypress(function(e){
        if(e.key == "Enter") {
            jQuery('#edit-validate-ga').click();
            return false;
        }
      });
});

$(document).ready(function() {
jQuery('#refresh_qr').click(function(e) {
        jQuery('#edit-configure-phone').click();
});

});


$(document).ready(function() {
jQuery('a[href=#forgot_password]').click(function(){
        jQuery('#edit-forgotpasswordform').click();
//          jQuery('#edit-login-submit').click();
});

});

}(jQuery));
