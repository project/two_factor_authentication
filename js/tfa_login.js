/**
 * @file
 * Js to handle default Drupal login and TFA login simultaneously .
 */

(function($) {
    Drupal.behaviors.tfa_login = {
        attach : function(context) {
            var loginElements = $('.form-item-name, .form-item-pass, li.tfa-link');
            var tfaElements = $('.form-item-tfa-identifier, li.user-link');
            var cookie = $.cookie('Drupal.visitor.tfa_identifier');

            // This behavior attaches by ID, so is only valid once on a page.
            if (!$('#edit-tfa-identifier.tfa-processed').length) {
                if (cookie) {
                    $('#edit-tfa-identifier').val(cookie);
                }
                if ($('#edit-tfa-identifier').val()
                    || location.hash == '#tfa-login'
                ) {
                    $('#edit-tfa-identifier').addClass('tfa-processed');
                    loginElements.hide();
                    // Use .css('display', 'block') instead of .show() to be Konqueror friendly.
                    tfaElements.css('display', 'block');
                }
                else {
                    tfaElements.hide();
                }
            }

            $('li.tfa-link:not(.tfa-processed)', context).addClass(
                'tfa-processed'
            ).click(
                function() {
                    loginElements.hide();
                    tfaElements.css('display', 'block');
                    // Remove possible error message.
                    $('#edit-name, #edit-pass').removeClass('error');
                    $('div.messages.error').hide();
                    // Set focus on OpenID Identifier field.
                    $('#edit-tfa-identifier')[0].focus();
                    return false;
                }
            );
            $('li.user-link:not(.tfa-processed)', context).addClass(
                'tfa-processed'
            ).click(
                function() {
                    tfaElements.hide();
                    loginElements.css('display', 'block');
                    // Clear OpenID Identifier field and remove possible error message.
                    $('#edit-tfa-identifier').val('').removeClass('error');
                    $('div.messages.error').css('display', 'block');
                    // Set focus on username field.
                    $('#edit-name')[0].focus();
                    return false;
                }
            );
        }
    };

})(jQuery);
