
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 * TFA login is 2 factor authenctication module used to 
   introduce second factor of authentication to default
   drupal login with help of miniorange auth API
   list of second factor authentication :
   1. Login with QR Code Scan
   2. Softtoken
   3. OTP on mail
   Note : It requires miniorange authentication APP

REQUIREMENTS
------------
This module requires the following modules:
 * user (Core module)
 * jQuery Update

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Download and install the jQuery Update module, and ensure that the Default jQuery Version is set to 1.10. Also ensure that the Alternate jQuery version for administrative pages is set to Default jQquery version or 1.10

CONFIGURATION
-------------
 * TFA Login module requires initial setup to used its second 
   factor of authentication

   Step 1. First you had to setup an account at miniorange using its API, 
           To do so you need to fill the form at 
           http://domain.com/user/1/tfa/user-profile.
   Step 2. On Success of step 1 , You will get Customer ID, API Key, 
           Token Key for miniorange which will automaticaly store 
           in drupal database as configuration.
   Step 3. Now you can install miniorange authenticater 
           APP in you mobile form playstore
        (https://play.google.com/store/apps/details?id=com.miniorange.authbeta)
        or form AppStore
    (https://itunes.apple.com/us/app/miniorange-authenticator/id796303566?ls=1)
   Step 4. Configure your mobile(http://domain.com/user/1/tfa/configure-mobile) 
           against an account by scanning QR Code using miniorange authenticater 
           APP (Also can test if configuration is correct on the same page)
   Step 5. You can make some global configuration from 
            http://domain.com/admin/tfa.
   Step 6. Now you can use second factor login in your drupal site
           (Make sure to do all these step against each user to setup 2 factor)

MAINTAINER
-----------
Current maintainers:
 * Anup Singh (anup.singh) - https://www.drupal.org/user/1737556
 * Makarand Chavan (cmak) - https://www.drupal.org/u/cmak
 * Saurabh Tripathi (saurabh.tripathi.cs) - https://www.drupal.org/u/saurabh.tripathi.cs
