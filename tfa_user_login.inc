<?php

/**
 * @file
 * Miniorange 2 factor user and admin mobile setup pages.
 */

/**
 * Implement login page to use QR Code, Softtoken, Timebased OTP.
 */
function tfa_user_login_2_facter($form, &$form_state, $user) {

  global $base_url;
  if (empty($form_state['input'])) {
    unset($_SESSION['auth']);
  }

  if (user_is_logged_in() || (user_is_anonymous() && empty($_SESSION['mo2f']))) {
    drupal_goto('user');
    exit;
  }

  $form_state['uid'] = $user->uid;
  $path = drupal_get_path('module', 'two_factor_authentication');
  $url = variable_get('two_factor_authentication_mo2f_host_name');

  if (!empty($_SESSION['mo2f']['mo2f-login-transactionId'])) {
    $form['#attached']['js'][] = array(
      'data' => array(
        'two_factor_authentication' => array(
          'mo2f_qrCode' => $_SESSION['mo2f']['mo2f-login-transactionId'],
          'path' => $path,
          'url' => $url,
        ),
      ),
      'type' => 'setting',
    );
  }

  // Display token validate form to the user in case status is either
  // MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL or MO_2_FACTOR_CHALLENGE_SOFT_TOKEN.
  $login_status = '';
  if (isset($_SESSION['mo2f']['mo_2factor_login_status'])) {
    $login_status = $_SESSION['mo2f']['mo_2factor_login_status'];
  }
  if (in_array(
        $login_status, array(
          'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL',
          'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN',
        )
    )) {
    if ($_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL') {
      $email = $user->field_miniorange_email[LANGUAGE_NONE][0]['value'];
      $sec_email = strstr($email, '@', TRUE);
      $first = substr($sec_email, 0, 1);
      $last = substr($sec_email, -1);
      $after = substr($email, strpos($email, "@") + 1);
      $sec_email = $first . 'xxxxxx' . $last . '@' . $after;
    }
    elseif ($_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN') {
      $form['help_text'] = array(
        '#type' => 'item',
        '#markup' => '<div class ="showOTP">Please enter the one time passcode shown in the miniOrange authenticator app.</div>
        <center><a href="#showOTPHelp" id="otpHelpLink"><h3>See How It Works ?</h3></a></center><br />
<div id="showOTPHelp" class="showOTPHelp" hidden>
        <br>
          <center><a href="#showOTP" id="otpLink"><h3>←Go Back</h3></a>
        <br>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->

            <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>

            </ol>
            <div class="carousel-inner" role="listbox">


               <div class="item active">
               <p>Open miniOrange Authenticator app and click on settings icon on top right corner.</p><br>
              <img class="first-slide" src="http://miniorange.com/images/help/qr-help-2.png" alt="First slide">
              </div>
               <div class="item">
               <p>Click on Sync button below to sync your time with miniOrange Servers. This is a one time sync to avoid otp validation failure.</p><br>
              <img class="first-slide" src="http://miniorange.com/images/help/token-help-3.png" alt="First slide">
              </div>
              <div class="item">
               <p>Go to Soft Token tab.</p><br>
              <img class="first-slide" src="http://miniorange.com/images/help/token-help-2.png" alt="First slide">
              </div>
              <div class="item">
               <p>Enter the one time passcode shown in miniOrange Authenticator app here.</p><br>
              <img class="first-slide" src="http://miniorange.com/images/help/token-help-4.png" alt="First slide">
              </div>
            </div>

        </div>
        </div>  </div>
      </div>',
      );
    }

    $form['otp'] = array(
      '#type' => 'password',
      '#attributes' => array('placeholder' => t('Enter OTP')),
      '#prefix' => '<div class="showOTP"><div class= "resend-otp"></div>',

    );

    $form['validate_otp'] = array(
      '#type' => 'submit',
      '#value' => t('Validate'),
      '#suffix' => l(t('&#8592; Back to login'), 'user', array('absolute' => TRUE, 'html' => TRUE)) . '</div>',
    );

    if ($_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL') {
      drupal_add_library('system', 'drupal.ajax');
      $output = l(t('&#8592; Back to login'), 'user', array('absolute' => TRUE, 'html' => TRUE));
      $form['validate_otp']['#suffix'] = $output;
    }
  }
  // Display QR Code Scan form to login through miniorange authenticater.
  else {
    if ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
      $form['header']['#markup'] = t(
            '<h4>Test Email Verification</h4><div class="email-wrap email-verification"><h6>A verification email is sent to your registered email.
    We are waiting for your approval...</h6>'
        );
      $image_path = file_create_url($base_url . '/' . drupal_get_path('module', 'two_factor_authentication') . '/images/ajax-loader-login.gif');
      $form['loader']['#markup'] = '<div><img src="' . $image_path . '"></div>';
      $form['qr_scan_submit'] = array(
        '#type' => 'submit',
        '#value' => t('QR Submit'),
        '#attributes' => array('class' => array('element-invisible')),
      );

      $form['email_login_failed'] = array(
        '#type' => 'submit',
        '#value' => t('Failed'),
        '#attributes' => array('class' => array('element-invisible')),
      );
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR) {
      if (empty($_SESSION['auth'])) {
        drupal_set_message("Please enter the one time passcode shown in the Google Authenticator/Authy app.");
        $_SESSION['auth'] = 'auth';
      }
      $form['ga_otp'] = array(
        '#type' => 'textfield',
        '#title' => 'Enter one time passcode',
        '#placeholder' => t('Enter OTP'),
        '#element_validate' => array('element_validate_integer_positive'),
      );

      $form['validate_ga'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
        '#suffix' => '</div>',
      );
      // Hide forgot phone option while login.
      // In case its disabled at admin configuration.
      if (variable_get('two_factor_authentication_mo2f_enable_forgotphone')) {
        $form['forgot_phone'] = array(
          '#type' => 'submit',
          '#value' => t('Forgot Phone?'),
        );
      }
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION) {
      $data = $_SESSION['mo2f']['mo2f-login-qrCode'];
      $form['qr_code'] = array(
        '#type' => 'item',
        '#markup' => '<center><a href="#" id="helpLink"><h3><div class="qr_validate">See How It Works ?</div></h3></a></center>
            <div style="margin-bottom:5%;">
            <center>
            <h3><div class="qr_validate">Identify yourself by scanning the QR code with miniOrange Authenticator app.</h3>
            </center></div></div>
                                        <div id="showQRHelp" class="showQRHelp" hidden>
          <br>
          <center><a href="#showQRHelp" id="qrLink"><h3>←Back to Scan QR Code.</h3></a>
          <br>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
              </ol>
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img class="first-slide" src="http://miniorange.com/images/help/qr-help-1.png" alt="First slide">
              </div>
               <div class="item">
              <p>Open miniOrange Authenticator app and click on Authenticate.</p><br>
              <img class="first-slide" src="http://miniorange.com/images/help/qr-help-2.png" alt="First slide">

              </div>
              <div class="item">
              <img class="first-slide" src="http://miniorange.com/images/help/qr-help-3.png" alt="First slide">
              </div>
              <div class="item">
              <img class="first-slide" src="http://miniorange.com//images/help/qr-help-4.png" alt="First slide">
              </div>
              <div class="item">
              <img class="first-slide" src="http://miniorange.com/images/help/qr-help-5.png" alt="First slide">
              </div>
            </div>
            </div>
          </center>
        </div>

              </div>
    </div>

            <div id="displayQrCode" class="qr_validate" style="margin-bottom:5%;"><img src="data:image/jpg;base64,' . $data . '" />
            </div>
          </div>',
      );
      // Hide forgot phone option while login.
      // In case its disabled at admin configuration.
      if (variable_get('two_factor_authentication_mo2f_enable_forgotphone')) {
        $form['forgot_phone'] = array(
          '#prefix' => '<div class="qr-actions"><div class="qr_validate">',
          '#suffix' => '</div>',
          '#type' => 'submit',
          '#value' => t('Forgot Phone?'),
        );
      }

      $form['phone_is_offline'] = array(
        '#prefix' => '<div class="qr_validate">',
        '#suffix' => '</div>',
        '#type' => 'submit',
        '#value' => t('Phone is Offline?'),
      );

      $form['qr_scan_submit'] = array(
        '#type' => 'submit',
        '#value' => t('QR Submit'),
        '#attributes' => array('class' => array('element-invisible')),
      );
    }
    else {
      drupal_set_message(t("You haven't configured authentication method properly, please contact administrator"), 'error');
    }

    $form['back_to_login'] = array(
      '#prefix' => '<div class="qr_validate">',
      '#suffix' => '</div>',
      '#type' => 'submit',
      '#value' => t('←Back To Login'),
      '#suffix' => '</div>',
    );
  }

  return $form;
}

/**
 * Hook submit to perform login task.
 */
function tfa_user_login_2_facter_submit($form, &$form_state) {

  $form_state['rebuild'] = TRUE;
  $user = $form_state['build_info']['args'][0];
  if (isset($form_state['triggering_element'])) {
    $email = $user->field_miniorange_email[LANGUAGE_NONE][0]['value'];
    $sec_email = strstr($email, '@', TRUE);
    $first = substr($sec_email, 0, 1);
    $last = substr($sec_email, -1);
    $after = substr($email, strpos($email, "@") + 1);
    $sec_email = $first . 'xxxxxx' . $last . '@' . $after;
    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'forgot_phone') {
      // Send OTP over mail and set session to show OTP validate form.
      $customer = new customer_setup();
      $content = json_decode($customer->send_otp_token($email, 'EMAIL', variable_get('two_factor_authentication_mo2f_customerKey'), variable_get('two_factor_authentication_mo2f_api_key')), TRUE);

      if (strcasecmp($content['status'], 'SUCCESS') == 0) {
        unset($_SESSION['mo2f']);
        drupal_set_message(
              t(
                  'A one time passcode has been sent to :email . Please enter the OTP to verify your identity.', array(
                    ':email' => $sec_email,
                  )
              )
          );
        $_SESSION['mo2f']['mo2f-login-transactionId'] = $content['txId'];
        $_SESSION['mo2f']['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL';
      }
      else {
        drupal_set_message(t('Error OTP over Email'), 'error');
      }
    }
    elseif (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'phone_is_offline') {
      // Set session to show Softtoken validate form.
      $_SESSION['mo2f']['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN';
    }
    elseif (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'qr_scan_submit') {
      // Validate if QR code is successfully scaned using
      // miniorange authenticater APP.
      $check_mobile_status = new two_factor_setup();

      if ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
        $content = $check_mobile_status->check_mobile_status($_SESSION['mo2f']['mo2f-login-transactionId']);
      }
      else {
        $content = $check_mobile_status->check_mobile_status($_SESSION['mo2f']['mo2f-login-transactionId']);
      }
      $response = json_decode($content, TRUE);

      if (json_last_error() == JSON_ERROR_NONE) {
        if ($response['status'] == 'SUCCESS') {
          user_login_submit(array(), $form_state);
          if ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
            drupal_set_message(t('Logged in successfully with Email Verification'), 'status');
          }
          else {
            drupal_set_message(t('Logged in successfully with QR Code Authentication'), 'status');
          }
        }
        else {
          form_set_error(t('Invalid session submited'));
        }
      }
      else {
        form_set_error(t('Invalid request.'));
      }
    }
    elseif (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'email_login_failed') {
      drupal_set_message(t('Invalid request.'), 'error');
      drupal_goto('user');
    }
    // Unset session and redirect to user login form.
    elseif (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'back_to_login') {
      unset($_SESSION['mo2f']);
      drupal_goto('user');
    }
    elseif (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'validate_otp') {
      $customer = new customer_setup();

      $softtoken = $form_state['values']['otp'];
      // Validate OTP over mail.
      if (isset($_SESSION['mo2f']['mo_2factor_login_status']) && $_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL') {
        $content = json_decode($customer->validate_otp_token('EMAIL', NULL, $_SESSION['mo2f']['mo2f-login-transactionId'], $softtoken), TRUE);
      }
      // Validate Softtoken against the email.
      elseif (isset($_SESSION['mo2f']['mo_2factor_login_status']) && $_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN') {
        $content = json_decode($customer->validate_otp_token('SOFT TOKEN', $email, $_SESSION['mo2f']['mo2f-login-transactionId'], $softtoken), TRUE);
      }
      // If successfully validated the OTP, Login user into the system.
      if (strcasecmp($content['status'], 'SUCCESS') == 0) {
        // Login user to the system.
        user_login_submit(array(), $form_state);
      }
      else {
        $message = $_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN' ? 'Invalid OTP. <b>Please try again after clicking on the Settings icon in the app and press Sync button.</b>' : 'Invalid OTP. Please try again';
        $_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN' ? $_SESSION['mo2f']['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN' : $_SESSION['mo2f']['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL';
        drupal_set_message(t(':message', array(':message' => $message)), 'error');
      }
    }
    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'validate_ga') {
      $softtoken = $form_state['values']['ga_otp'];
      if (MO2f_utility::mo2f_check_empty_or_null($softtoken)) {
        drupal_set_message(t('Please enter OTP to proceed.'), 'error');
        return;
      }
      else {
        $softtoken = check_plain($softtoken);
        if (!MO2f_utility::mo2f_check_number_length($softtoken)) {
          drupal_set_message(t('Invalid OTP. Only digits within range 4-8 are allowed. Please try again.'), 'error');
          return;
        }
      }
      if (isset($form_state['values']['tfa_identifier']) && !empty($form_state['values']['tfa_identifier'])) {
        $username = '';
        $username = $form_state['values']['tfa_identifier'];
        $entered_user = user_load_by_name($username);
      }
      if (empty($entered_user)) {
        $customer = new Customer_Setup();
        $content = json_decode($customer->validate_otp_token('GOOGLE AUTHENTICATOR', $email, NULL, $softtoken), TRUE);
        if (strcasecmp($content['status'], 'SUCCESS') == 0) {
          // Login user to the system.
          user_login_submit(array(), $form_state);
          drupal_set_message(t('Logged in successfully with Google Authenticator'), 'status');
        }
        else {
          $message = $_SESSION['mo2f']['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN' ? 'Invalid OTP. <b>Please try again after clicking on the Settings icon in the app and press Sync button.</b>' : 'Invalid OTP. Please try again';
          drupal_set_message(t(':message', array(':message' => $message)), 'error');
        }
      }
      else {
        form_set_error('tfa_identifier', t('Please enter a valid username'));
      }
    }
  }
}
