<?php

/**
 * @file
 * MiniOrange enables user to log in through mobile authentication as an additional layer of security over password.
 * Copyright (C) 2015  miniOrange.
 *  * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package miniOrange OAuth
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 */

/**
 * This library is miniOrange Authentication Service.
 * Contains Request Calls to Customer service.
 **/
class MO2f_Utility {
  /**
   *
   */
  public static function get_hidden_phone($phone) {

    $hidden_phone = 'xxxxxxx' . substr($phone, strlen($phone) - 3);
    return $hidden_phone;
  }/**
    *
    */
  public static function mo2f_check_empty_or_null($value) {

    if (!isset($value) || empty($value)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   *
   */
  public static function is_curl_installed() {

    if (in_array('curl', get_loaded_extensions())) {
      return 1;
    }
    else {
      return 0;
    }
  }

  /**
   *
   */
  public static function mo2f_check_number_length($token) {

    if (is_numeric($token)) {
      if (strlen($token) >= 4 && strlen($token) <= 8) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   *
   */
  public static function mo2f_get_hiden_email($email) {

    if (!isset($email) || trim($email) === '') {
      return "";
    }
    $emailsize = strlen($email);
    $partialemail = substr($email, 0, 1);
    $temp = strrpos($email, "@");
    $endemail = substr($email, $temp - 1, $emailsize);
    for ($i = 1; $i < $temp; $i++) {
      $partialemail = $partialemail . 'x';
    }
    $hiddenemail = $partialemail . $endemail;

    return $hiddenemail;
  }

  /**
   *
   */
  public static function check_if_email_is_already_registered($email) {

    $users = get_users(array());
    foreach ($users as $user) {
      if (get_user_meta($user->ID, 'mo_2factor_map_id_with_email', TRUE) == $email) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
