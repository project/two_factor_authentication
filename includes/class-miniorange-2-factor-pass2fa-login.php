<?Php

/**
 * @file MiniOrange enables user to log in through mobile authentication as an additional layer of security over password.
 * Copyright (C) 2015  miniOrange.
 *  * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package miniOrange OAuth
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 */

/**
 * This library is miniOrange Authentication Service.
 * Contains Request Calls to Customer service. *  .**/
include_once dirname(__FILE__) . '/miniorange_2_factor_common_login.php';
include_once dirname(__FILE__) . '/class-rba-attributes.php';
/**
 *
 */
class Miniorange_Password_2Factor_Login{
  /**
   *
   */
  function remove_current_activity() {
    unset($_SESSION['mo2f_current_user']);
    unset($_SESSION['mo2f_1stfactor_status']);
    unset($_SESSION['mo_2factor_login_status']);
    unset($_SESSION['mo2f-login-qrCode']);
    unset($_SESSION['mo2f-login-transactionId']);
    unset($_SESSION['mo2f-login-message']);
    unset($_SESSION['mo2f_rba_status']);
  }

  /**
   *
   */
  function mo2fa_pass2login() {
    if (isset($_SESSION['mo2f_current_user']) && isset($_SESSION['mo2f_1stfactor_status']) && $_SESSION['mo2f_1stfactor_status'] = 'VALIDATE_SUCCESS') {
      $currentuser = $_SESSION['mo2f_current_user'];
      $user_id = $currentuser->ID;
      wp_set_current_user($user_id, $currentuser->user_login);
      $this->remove_current_activity();
      do_action('wp_login', $currentuser->user_login, $currentuser);
      wp_set_auth_cookie($user_id, TRUE);
      redirect_user_to($currentuser);
      exit;
    }
    else {
      $this->remove_current_activity();
    }
  }

  /**
   *
   */
  public function miniorange_pass2login_redirect() {
    if (!session_id()) {
      session_start();
    }

    if (isset($_POST['mo2f_trust_device_confirm_nonce'])) {
      /*register device as rba profile */

      $nonce = $_POST['mo2f_trust_device_confirm_nonce'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-trust-device-confirm-nonce')) {
        $this->remove_current_activity();
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {
        try {
          $currentuser = $_SESSION['mo2f_current_user'];
          mo2f_register_profile(get_user_meta($currentuser->ID, 'mo_2factor_map_id_with_email', TRUE), 'true', $_SESSION['mo2f_rba_status']);
        }
        catch (Exception$e) {
          echo $e->getMessage();
        }
        $this->mo2fa_pass2login();
      }
    }

    if (isset($_POST['mo2f_trust_device_cancel_nonce'])) {
      /*do not register device as rba profile */

      $nonce = $_POST['mo2f_trust_device_cancel_nonce'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-trust-device-cancel-nonce')) {
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {
        $this->mo2fa_pass2login();
      }
    }

    if (isset($_POST['miniorange_mobile_validation_nonce'])) {
      /*check mobile validation */

      $nonce = $_POST['miniorange_mobile_validation_nonce'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-mobile-validation-nonce')) {
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {

        $currentuser = $_SESSION['mo2f_current_user'];
        $checkMobileStatus = new Two_Factor_Setup();
        $content = $checkMobileStatus->check_mobile_status($_SESSION['mo2f-login-transactionId']);
        $response = json_decode($content, TRUE);
        if (json_last_error() == JSON_ERROR_NONE) {
          if ($response['status'] == 'SUCCESS') {
            if (get_option('mo2f_deviceid_enabled')) {
              $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_REMEMBER_TRUSTED_DEVICE';
            }
            else {
              $this->mo2fa_pass2login();
            }
          }
          else {
            $this->remove_current_activity();
            return new WP_Error('invalid_username', __('<strong>ERROR</strong>: Please try again.'));
          }
        }
        else {
          $this->remove_current_activity();
          return new WP_Error('invalid_username', __('<strong>ERROR</strong>: Please try again.'));
        }
      }
    }

    if (isset($_POST['miniorange_mobile_validation_failed_nonce'])) {
      /*Back to miniOrange Login Page if mobile validation failed and from back button of mobile challenge, soft token and default login*/

      $nonce = $_POST['miniorange_mobile_validation_failed_nonce'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-mobile-validation-failed-nonce')) {
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {
        $this->remove_current_activity();
      }
    }

    if (isset($_POST['miniorange_forgotphone'])) {
      /*Click on the link of forgotphone */

      $nonce = $_POST['miniorange_forgotphone'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-forgotphone')) {
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {
        $customer = new Customer_Setup();
        $id = $_SESSION['mo2f_current_user']->ID;
        $content = json_decode($customer->send_otp_token(get_user_meta($id, 'mo_2factor_map_id_with_email', TRUE), 'EMAIL', get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key')), TRUE);
        if (strcasecmp($content['status'], 'SUCCESS') == 0) {
          unset($_SESSION['mo2f-login-qrCode']);
          unset($_SESSION['mo2f-login-transactionId']);
          $_SESSION['mo2f-login-message'] = 'A one time passcode has been sent to <b>' . MO2f_Utility::mo2f_get_hiden_email(get_user_meta($id, 'mo_2factor_map_id_with_email', TRUE)) . '</b>. Please enter the OTP to verify your identity.';
          $_SESSION['mo2f-login-transactionId'] = $content['txId'];
          $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL';
        }
        else {
          $_SESSION['mo2f-login-message'] = 'Error:OTP over Email';
        }
      }
    }

    if (isset($_POST['miniorange_softtoken'])) {
      /*Click on the link of phone is offline */

      $nonce = $_POST['miniorange_softtoken'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-softtoken')) {
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {
        unset($_SESSION['mo2f-login-qrCode']);
        unset($_SESSION['mo2f-login-transactionId']);
        $_SESSION['mo2f-login-message'] = 'Please enter the one time passcode shown in the miniOrange authenticator app.';
        $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN';
      }
    }

    if (isset($_POST['miniorange_soft_token_nonce'])) {
      /*Validate Soft Token,OTP over SMS,OTP over EMAIL,Phone verification */

      $nonce = $_POST['miniorange_soft_token_nonce'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-soft-token-nonce')) {
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {
        $softtoken = '';
        if (MO2f_utility::mo2f_check_empty_or_null($_POST['mo2fa_softtoken'])) {
          $_SESSION['mo2f-login-message'] = 'Please enter OTP to proceed.';
          return;
        }
        else {
          $softtoken = sanitize_text_field($_POST['mo2fa_softtoken']);
          if (!MO2f_utility::mo2f_check_number_length($softtoken)) {
            $_SESSION['mo2f-login-message'] = 'Invalid OTP. Only digits within range 4-8 are allowed. Please try again.';
            return;
          }
        }
        $currentuser = isset($_SESSION['mo2f_current_user']) ? $_SESSION['mo2f_current_user'] : NULL;
        if (isset($_SESSION['mo2f_current_user'])) {
          $customer = new Customer_Setup();
          $content = '';
          if (isset($_SESSION['mo_2factor_login_status']) && $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL') {
            $content = json_decode($customer->validate_otp_token('EMAIL', NULL, $_SESSION['mo2f-login-transactionId'], $softtoken), TRUE);
          }
          elseif (isset($_SESSION['mo_2factor_login_status']) && $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_SMS') {
            $content = json_decode($customer->validate_otp_token('SMS', NULL, $_SESSION['mo2f-login-transactionId'], $softtoken), TRUE);
          }
          elseif (isset($_SESSION['mo_2factor_login_status']) && $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_PHONE_VERIFICATION') {
            $content = json_decode($customer->validate_otp_token('PHONE VERIFICATION', NULL, $_SESSION['mo2f-login-transactionId'], $softtoken), TRUE);
          }
          elseif (isset($_SESSION['mo_2factor_login_status']) && $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN') {
            $content = json_decode($customer->validate_otp_token('SOFT TOKEN', get_user_meta($currentuser->ID, 'mo_2factor_map_id_with_email', TRUE), NULL, $softtoken), TRUE);
          }
          elseif (isset($_SESSION['mo_2factor_login_status']) && $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_GOOGLE_AUTHENTICATION') {
            $content = json_decode($customer->validate_otp_token('GOOGLE AUTHENTICATOR', get_user_meta($currentuser->ID, 'mo_2factor_map_id_with_email', TRUE), NULL, $softtoken), TRUE);
          }
          else {
            $this->remove_current_activity();
            return new WP_Error('invalid_username', __('<strong>ERROR</strong>: Invalid Request. Please try again.'));
          }

          if (strcasecmp($content['status'], 'SUCCESS') == 0) {
            if (get_option('mo2f_deviceid_enabled')) {
              $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_REMEMBER_TRUSTED_DEVICE';
            }
            else {
              $this->mo2fa_pass2login();
            }
          }
          else {

            $message = $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN' ? 'Invalid OTP ...Possible causes <br />1. You mis-typed the OTP, find the OTP again and type it. <br /> 2. Your phone time is not in sync with miniOrange servers. <br /><b>How to sync?</b> In the app,tap on Settings icon and then press Sync button.' : 'Invalid OTP. Please try again';
            $_SESSION['mo2f-login-message'] = $message;
          }
        }
        else {
          $this->remove_current_activity();
          return new WP_Error('invalid_username', __('<strong>ERROR</strong>: Please try again..'));
        }
      }
    }
  }

  /**
   *
   */
  function mo2f_check_username_password($user, $username, $password) {

    if (isset($_POST['miniorange_login_nonce'])) {
      $nonce = $_POST['miniorange_login_nonce'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-login-nonce')) {
        wp_logout();
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: Invalid Request.'));
        return $error;
      }
      else {
        $currentuser = mo2f_wp_authenticate_username_password($user, $username, $password);
        if (is_wp_error($currentuser)) {
          return $currentuser;
        }
        else {
          $_SESSION['mo2f_current_user'] = $currentuser;
          $_SESSION['mo2f_1stfactor_status'] = 'VALIDATE_SUCCESS';
          // Plugin is activated for admin always.
          if (!strcasecmp(wp_sprintf_l('%l', $currentuser->roles), 'administrator')) {
            if (!session_id()) {
              session_start();
            }
            $email = get_user_meta($currentuser->ID, 'mo_2factor_map_id_with_email', TRUE);
            $attributes = isset($_POST['miniorange_rba_attribures']) ? $_POST['miniorange_rba_attribures'] : NULL;
            // Checking if user has configured any 2nd factor method.
            if ($email && get_user_meta($currentuser->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS') {

              try {
                // Rba flow.
                $mo2f_rba_status = mo2f_collect_attributes($email, stripslashes($attributes));
              }
              catch (Exception$e) {
                echo $e->getMessage();
              }

              if ($mo2f_rba_status['status'] == 'SUCCESS' && $mo2f_rba_status['decision_flag']) {
                $this->mo2fa_pass2login();
              }
              else {
                $_SESSION['mo2f_rba_status'] = $mo2f_rba_status;
                $mo2f_second_factor = mo2f_get_user_2ndfactor($currentuser);
                if ($mo2f_second_factor == 'MOBILE AUTHENTICATION') {
                  $this->mo2f_pass2login_mobile_verification($currentuser);
                }
                elseif ($mo2f_second_factor == 'PUSH NOTIFICATIONS' || $mo2f_second_factor == 'OUT OF BAND EMAIL') {
                  $this->mo2f_pass2login_push_oobemail_verification($currentuser, $mo2f_second_factor);
                }
                elseif ($mo2f_second_factor == 'SOFT TOKEN' || $mo2f_second_factor == 'SMS' || $mo2f_second_factor == 'PHONE VERIFICATION' || $mo2f_second_factor == 'GOOGLE AUTHENTICATOR') {
                  $this->mo2f_pass2login_otp_verification($currentuser, $mo2f_second_factor);
                }
                else {
                  $this->remove_current_activity();
                  $error = new WP_Error();
                  $error->add('empty_username', __('<strong>ERROR</strong>: Please try again or contact your admin.'));
                  return $error;
                }
              }
              // If user has not configured any 2nd factor method then logged him in without asking 2nd factor.
            }
            else {
              $this->mo2fa_pass2login();
            }
          }
          else {
            if (!get_option('two_factor_authentication_mo2f_admin_disabled_status')) {
              /*checking if plugin is activated for all other roles */

              if (!session_id()) {
                session_start();
              }

              $email = get_user_meta($currentuser->ID, 'mo_2factor_map_id_with_email', TRUE);
              $attributes = isset($_POST['miniorange_rba_attribures']) ? $_POST['miniorange_rba_attribures'] : NULL;

              // Checking if user has configured any 2nd factor method.
              if ($email && get_user_meta($currentuser->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS') {

                try {
                  // Rba flow.
                  $mo2f_rba_status = mo2f_collect_attributes($email, stripslashes($attributes));
                }
                catch (Exception$e) {
                  echo $e->getMessage();
                }

                if ($mo2f_rba_status['status'] == 'SUCCESS' && $mo2f_rba_status['decision_flag']) {
                  $this->mo2fa_pass2login();
                }
                else {
                  $_SESSION['mo2f_rba_status'] = $mo2f_rba_status;
                  $mo2f_second_factor = mo2f_get_user_2ndfactor($currentuser);
                  if ($mo2f_second_factor == 'MOBILE AUTHENTICATION') {
                    $this->mo2f_pass2login_mobile_verification($currentuser);
                  }
                  elseif ($mo2f_second_factor == 'PUSH NOTIFICATIONS' || $mo2f_second_factor == 'OUT OF BAND EMAIL') {
                    $this->mo2f_pass2login_push_oobemail_verification($currentuser, $mo2f_second_factor);
                  }
                  elseif ($mo2f_second_factor == 'SOFT TOKEN' || $mo2f_second_factor == 'SMS' || $mo2f_second_factor == 'PHONE VERIFICATION' || $mo2f_second_factor == 'GOOGLE AUTHENTICATOR') {
                    $this->mo2f_pass2login_otp_verification($currentuser, $mo2f_second_factor);
                  }
                  else {
                    $this->remove_current_activity();
                    $error = new WP_Error();
                    $error->add('empty_username', __('<strong>ERROR</strong>: Please try again or contact your admin.'));
                    return $error;
                  }
                }

                // If user has not configured any 2nd factor method then logged him in without asking 2nd factor.
              }
              else {
                $this->mo2fa_pass2login();
              }
              // Plugin is not activated for non-admin then logged him in.
            }
            else {
              $this->mo2fa_pass2login();
            }
          }
        }
      }
    }
    else {
      $error = new WP_Error();
      return $error;
    }
  }

  /**
   *
   */
  function mo_2_factor_enable_jquery() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap_script', plugins_url('includes/js/bootstrap.min.js', __FILE__));
  }

  /**
   *
   */
  function mo_2_factor_pass2login_hide_login() {
    wp_register_style('hide-login', plugins_url('includes/css/hide-login.css', __FILE__));
    wp_enqueue_style('hide-login');
    wp_register_style('bootstrap', plugins_url('includes/css/bootstrap.min.css?version=3.0', __FILE__));
    wp_enqueue_style('bootstrap');
  }

  /**
   *
   */
  function mo_2_factor_pass2login_show_login() {
    wp_register_style('show-login', plugins_url('includes/css/show-login.css', __FILE__));
    wp_enqueue_style('show-login');
  }

  /**
   *
   */
  function miniorange_pass2login_header_field() {
    ?>
	<script>
		var relPath = '<?php echo plugins_url('includes/js/rba/js', __FILE__); ?>';
	</script>
  <?php
  }

  /**
   *
   */
  function miniorange_pass2login_form_fields() {
    $login_status = isset($_SESSION['mo_2factor_login_status']) ? $_SESSION['mo_2factor_login_status'] : NULL;
    // For mobile.
    if ($this->miniorange_pass2login_check_mobile_status($login_status)) {
      $this->mo_2_factor_pass2login_hide_login();
      $this->mo_2_factor_pass2login_show_qr_code();
      // For soft-token,otp over email,sms,phone verification.
    }
    elseif ($this->miniorange_pass2login_check_otp_status($login_status)) {
      $this->mo_2_factor_pass2login_hide_login();
      $this->mo_2_factor_pass2login_show_otp_token();
      // For push and out of band email.
    }
    elseif ($this->miniorange_pass2login_check_push_oobemail_status($login_status)) {
      $this->mo_2_factor_pass2login_hide_login();
      $this->mo_2_factor_pass2login_show_push_oobemail();
    }
    elseif ($this->miniorange_pass2login_check_trusted_device_status($login_status)) {
      $this->mo_2_factor_pass2login_hide_login();
      $this->mo_2_factor_pass2login_show_device_page();
      // Show login screen.
    }
    else {
      $this->mo_2_factor_pass2login_show_login();
      $this->mo_2_factor_pass2login_show_wp_login_form();
    }
  }

  /**
   * Woocommerce front end login.
   */
  function miniorange_pass2login_form_fields_frontend() {
    $login_status = isset($_SESSION['mo_2factor_login_status']) ? $_SESSION['mo_2factor_login_status'] : NULL;
    // For mobile.
    if ($this->miniorange_pass2login_check_mobile_status($login_status)) {
      mo2f_frontend_getqrcode();
      // For soft-token,otp over email,sms,phone verification.
    }
    elseif ($this->miniorange_pass2login_check_otp_status($login_status)) {
      mo2f_frontend_getotp_form();
      // For push and out of band email.
    }
    elseif ($this->miniorange_pass2login_check_push_oobemail_status($login_status)) {
      mo2f_frontend_getpush_oobemail_response();
    }
    elseif ($this->miniorange_pass2login_check_trusted_device_status($login_status)) {
      mo2f_frontend_get_trusted_device_form();
    }
  }

  /**
   *
   */
  function miniorange_pass2login_check_trusted_device_status($login_status) {

    if ($login_status == 'MO_2_FACTOR_REMEMBER_TRUSTED_DEVICE') {
      $nonce = '';
      if (isset($_POST['miniorange_soft_token_nonce'])) {
        $nonce = $_POST['miniorange_soft_token_nonce'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-soft-token-nonce')) {
          return TRUE;
        }
      }
      elseif (isset($_POST['miniorange_mobile_validation_nonce'])) {
        $nonce = $_POST['miniorange_mobile_validation_nonce'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-mobile-validation-nonce')) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  function miniorange_pass2login_check_push_oobemail_status($login_status) {
    if ($login_status == 'MO_2_FACTOR_CHALLENGE_PUSH_NOTIFICATIONS' || $login_status == 'MO_2_FACTOR_CHALLENGE_OOB_EMAIL') {
      $nonce = '';

      if (isset($_POST['miniorange_login_nonce'])) {
        $nonce = $_POST['miniorange_login_nonce'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-login-nonce')) {
          return TRUE;
        }
      }
      elseif (isset($_POST['miniorange_forgotphone'])) {
        $nonce = $_POST['miniorange_forgotphone'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-forgotphone')) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  function miniorange_pass2login_check_otp_status($login_status) {
    if ($login_status == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN' || $login_status == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL' || $login_status == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_SMS' || $login_status == 'MO_2_FACTOR_CHALLENGE_PHONE_VERIFICATION' || $login_status == 'MO_2_FACTOR_CHALLENGE_GOOGLE_AUTHENTICATION') {
      $nonce = '';

      if (isset($_POST['miniorange_login_nonce'])) {
        $nonce = $_POST['miniorange_login_nonce'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-login-nonce')) {
          return TRUE;
        }
      }
      if (isset($_POST['miniorange_softtoken'])) {
        $nonce = $_POST['miniorange_softtoken'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-softtoken')) {
          return TRUE;
        }
      }
      elseif (isset($_POST['miniorange_forgotphone'])) {
        $nonce = $_POST['miniorange_forgotphone'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-forgotphone')) {
          return TRUE;
        }
      }
      elseif (isset($_POST['miniorange_soft_token_nonce'])) {
        $nonce = $_POST['miniorange_soft_token_nonce'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-soft-token-nonce')) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  function miniorange_pass2login_check_mobile_status($login_status) {
    if ($login_status == 'MO_2_FACTOR_CHALLENGE_MOBILE_AUTHENTICATION') {
      $nonce = '';
      if (isset($_POST['miniorange_login_nonce'])) {
        $nonce = $_POST['miniorange_login_nonce'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-login-nonce')) {
          return TRUE;
        }
      }
      elseif (isset($_POST['miniorange_forgotphone'])) {
        $nonce = $_POST['miniorange_forgotphone'];
        if (wp_verify_nonce($nonce, 'miniorange-2-factor-forgotphone')) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   *
   */
  function miniorange_pass2login_footer_form() {

    // Show these forms after default login form.
    if (isset($_SESSION['mo_2factor_login_status'])) {
      ?>
		<form name="f" id="mo2f_show_softtoken_loginform" method="post" action="" style="display:none;">
			<input type="hidden" name="miniorange_softtoken" value="<?php echo wp_create_nonce('miniorange-2-factor-softtoken'); ?>" />
		</form>
		<form name="f" id="mo2f_show_forgotphone_loginform" method="post" action="" style="display:none;">
			<input type="hidden" name="miniorange_forgotphone" value="<?php echo wp_create_nonce('miniorange-2-factor-forgotphone'); ?>" />
		</form>
		<form name="f" id="mo2f_backto_mo_loginform" method="post" action="<?php echo wp_login_url(); ?>" style="display:none;">
			<input type="hidden" name="miniorange_mobile_validation_failed_nonce" value="<?php echo wp_create_nonce('miniorange-2-factor-mobile-validation-failed-nonce'); ?>" />
		</form>
		<?php
      // Show this form when 2nd factor is mobile,email verification,push.
    }if (isset($_SESSION['mo_2factor_login_status']) && ($_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_MOBILE_AUTHENTICATION' || $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OOB_EMAIL' || $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_PUSH_NOTIFICATIONS')) {
      ?>
		<form name="f" id="mo2f_mobile_validation_form" method="post" action="" style="display:none;">
			<input type="hidden" name="miniorange_mobile_validation_nonce" value="<?php echo wp_create_nonce('miniorange-2-factor-mobile-validation-nonce'); ?>" />
		</form>
		<?php
      // Show this form when 2nd factor is otp over email(forgot phone),otp over sms,phone verification,soft token,google authenticator.
    }if (isset($_SESSION['mo_2factor_login_status']) && ($_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_EMAIL' || $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_OTP_OVER_SMS' || $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_PHONE_VERIFICATION' || $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN' || $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_CHALLENGE_GOOGLE_AUTHENTICATION')) {
      ?>
		<form name="f" id="mo2f_submitotp_loginform" method="post" action="" style="display:none;">
			<input type="text" name="mo2fa_softtoken" id="mo2fa_softtoken" hidden/>
			<input type="hidden" name="miniorange_soft_token_nonce" value="<?php echo wp_create_nonce('miniorange-2-factor-soft-token-nonce'); ?>" />
		</form>
		<?php
    // Show this form and script only rba is on.
    }
    if (get_option('mo2f_deviceid_enabled') && get_option('mo2f_login_policy')) {
      // Show this form only when rba is on and device is not trusted.
      if (isset($_SESSION['mo_2factor_login_status']) && $_SESSION['mo_2factor_login_status'] == 'MO_2_FACTOR_REMEMBER_TRUSTED_DEVICE') {
        ?>

			<form name="f" id="mo2f_trust_device_confirm_form" method="post" action="" style="display:none;">
				<input type="hidden" name="mo2f_trust_device_confirm_nonce" value="<?php echo wp_create_nonce('miniorange-2-factor-trust-device-confirm-nonce'); ?>" />
			</form>
			<form name="f" id="mo2f_trust_device_cancel_form" method="post" action="" style="display:none;">
				<input type="hidden" name="mo2f_trust_device_cancel_nonce" value="<?php echo wp_create_nonce('miniorange-2-factor-trust-device-cancel-nonce'); ?>" />
			</form>
    <?php
      } ?>

			<script>
			jQuery(document).ready(function(){
				if(document.getElementById('loginform') != null){
					 jQuery('#loginform').on('submit', function(e){
						jQuery('#miniorange_rba_attribures').val(JSON.stringify(rbaAttributes.attributes));
					});
				}else{
					if(document.getElementsByClassName('login') != null){
						jQuery('.login').on('submit', function(e){
							jQuery('#miniorange_rba_attribures').val(JSON.stringify(rbaAttributes.attributes));
						});
					}
				}
			});
			</script>
  <?php
    }
  }

  /**
   *
   */
  function mo2f_pass2login_otp_verification($user, $mo2f_second_factor) {
    if ($mo2f_second_factor == 'SOFT TOKEN') {
      $_SESSION['mo2f-login-message'] = 'Please enter the one time passcode shown in the <b>miniOrange Authenticator</b> app.';
      $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN';
    }
    elseif ($mo2f_second_factor == 'GOOGLE AUTHENTICATOR') {
      $_SESSION['mo2f-login-message'] = 'Please enter the one time passcode shown in the <b>Google Authenticator</b> app.';
      $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_GOOGLE_AUTHENTICATION';
    }
    else {
      $challengeMobile = new Customer_Setup();
      $content = $challengeMobile->send_otp_token(get_user_meta($user->ID, 'mo_2factor_map_id_with_email', TRUE), $mo2f_second_factor, get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key'));
      $response = json_decode($content, TRUE);
      if (json_last_error() == JSON_ERROR_NONE) {
        if ($response['status'] == 'SUCCESS') {
          $message = $mo2f_second_factor == 'SMS' ? 'The OTP has been sent to ' . MO2f_Utility::get_hidden_phone($response['phoneDelivery']['contact']) . '. Please enter the OTP you received to Validate.' : 'You will receive phone call on ' . MO2f_Utility::get_hidden_phone($response['phoneDelivery']['contact']) . ' with OTP. Please enter the OTP to Validate.';
          $_SESSION['mo2f-login-message'] = $message;
          $_SESSION['mo2f-login-transactionId'] = $response['txId'];
          $_SESSION['mo_2factor_login_status'] = $mo2f_second_factor == 'SMS' ? 'MO_2_FACTOR_CHALLENGE_OTP_OVER_SMS' : 'MO_2_FACTOR_CHALLENGE_PHONE_VERIFICATION';
        }
        else {
          $this->remove_current_activity();
          $error = new WP_Error();
          $error->add('empty_username', __('<strong>ERROR</strong>: An error occured while processing your request. Please Try again.'));
          return $error;
        }
      }
      else {
        $this->remove_current_activity();
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: An error occured while processing your request. Please Try again.'));
        return $error;
      }
    }
  }

  /**
   *
   */
  function mo2f_pass2login_push_oobemail_verification($user, $mo2f_second_factor) {
    $challengeMobile = new Customer_Setup();
    $content = $challengeMobile->send_otp_token(get_user_meta($user->ID, 'mo_2factor_map_id_with_email', TRUE), $mo2f_second_factor, get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key'));
    $response = json_decode($content, TRUE);
    if (json_last_error() == JSON_ERROR_NONE) {
      /* Generate Qr code */

      if ($response['status'] == 'SUCCESS') {
        $_SESSION['mo2f-login-transactionId'] = $response['txId'];
        $_SESSION['mo2f-login-message'] = $mo2f_second_factor == 'PUSH NOTIFICATIONS' ? 'A Push Notification has been sent to your phone. We are waiting for your approval.' : 'An email has been sent to ' . MO2f_Utility::mo2f_get_hiden_email(get_user_meta($user->ID, 'mo_2factor_map_id_with_email', TRUE)) . '. We are waiting for your approval.';
        $_SESSION['mo_2factor_login_status'] = $mo2f_second_factor == 'PUSH NOTIFICATIONS' ? 'MO_2_FACTOR_CHALLENGE_PUSH_NOTIFICATIONS' : 'MO_2_FACTOR_CHALLENGE_OOB_EMAIL';
      }
      elseif ($response['status'] == 'ERROR' || $response['status'] == 'FAILED') {
        $this->remove_current_activity();
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: An error occured while processing your request. Please Try again.'));
        return $error;
      }
    }
    else {
      $this->remove_current_activity();
      $error = new WP_Error();
      $error->add('empty_username', __('<strong>ERROR</strong>: An error occured while processing your request. Please Try again.'));
      return $error;
    }
  }

  /**
   *
   */
  function mo2f_pass2login_mobile_verification($user) {

    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (strpos($useragent, 'Mobi') !== FALSE) {
      unset($_SESSION['mo2f-login-qrCode']);
      unset($_SESSION['mo2f-login-transactionId']);
      $_SESSION['mo2f-login-message'] = 'Please enter the one time passcode shown in the miniOrange Authenticator app.';
      $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_SOFT_TOKEN';
    }
    else {
      $challengeMobile = new Customer_Setup();
      $content = $challengeMobile->send_otp_token(get_user_meta($user->ID, 'mo_2factor_map_id_with_email', TRUE), 'MOBILE AUTHENTICATION', get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key'));
      $response = json_decode($content, TRUE);
      if (json_last_error() == JSON_ERROR_NONE) {
        /* Generate Qr code */

        if ($response['status'] == 'SUCCESS') {
          $_SESSION['mo2f-login-qrCode'] = $response['qrCode'];
          $_SESSION['mo2f-login-transactionId'] = $response['txId'];
          $_SESSION['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_MOBILE_AUTHENTICATION';
        }
        elseif ($response['status'] == 'ERROR') {
          $this->remove_current_activity();
          $error = new WP_Error();
          $error->add('empty_username', __('<strong>ERROR</strong>: An error occured while processing your request. Please Try again.'));
          return $error;
        }
      }
      else {
        $this->remove_current_activity();
        $error = new WP_Error();
        $error->add('empty_username', __('<strong>ERROR</strong>: An error occured while processing your request. Please Try again.'));
        return $error;
      }
    }
  }

  /**
   *
   */
  function mo_2_factor_pass2login_show_wp_login_form() {
    ?>
		<p><input type="hidden" name="miniorange_login_nonce" value="<?php echo wp_create_nonce('miniorange-2-factor-login-nonce'); ?>" /></p>
		<?php if (get_option('mo2f_deviceid_enabled')) {
?>
				<p><input type="hidden" id="miniorange_rba_attribures" name="miniorange_rba_attribures" value="" /></p>
		<?php
    wp_enqueue_script('jquery_script', plugins_url('includes/js/rba/js/jquery-1.9.1.js', __FILE__)); wp_enqueue_script('flash_script', plugins_url('includes/js/rba/js/jquery.flash.js', __FILE__)); wp_enqueue_script('client_script', plugins_url('includes/js/rba/js/client.js', __FILE__));
  wp_enqueue_script('device_script', plugins_url('includes/js/rba/js/device_attributes.js', __FILE__));
  wp_enqueue_script('font_script', plugins_url('includes/js/rba/js/fontdetect.js', __FILE__));
  wp_enqueue_script('miniorange_script', plugins_url('includes/js/rba/js/miniorange-fp.js', __FILE__));
  wp_enqueue_script('murmur_script', plugins_url('includes/js/rba/js/murmurhash3.js', __FILE__));
  wp_enqueue_script('swf_script', plugins_url('includes/js/rba/js/swfobject.js', __FILE__));
  wp_enqueue_script('uaparser_script', plugins_url('includes/js/rba/js/ua-parser.js', __FILE__));
}
  }

  /**
   * For mobile authentication.
   */
  function mo_2_factor_pass2login_show_qr_code() {
    mo2f_getqrcode();
  }

  /**
   * For soft token,sms,email(forgot phone),phone verification.
   */
  function mo_2_factor_pass2login_show_otp_token() {
    mo2f_getotp_form();
  }

  /**
   * For push notification and out of band email.
   */
  function mo_2_factor_pass2login_show_push_oobemail() {
    mo2f_getpush_oobemail_response();
  }

  /**
   *
   */
  function mo_2_factor_pass2login_show_device_page() {
    mo2f_get_device_form();
  }

}
