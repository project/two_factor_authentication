<?php

/**
 * @file
 * MiniOrange enables user to log in through mobile authentication as an additional layer of security over password.
 * Copyright (C) 2015  miniOrange.
 *  * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package miniOrange OAuth
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 */

/**
 * This library is miniOrange Authentication Service.
 * Contains Request Calls to Customer service.
 **/
class Two_Factor_Setup {
  public $email; /**
                  *
                  */
  function check_mobile_status($tId) {

    if (!MO2f_Utility::is_curl_installed()) {
      $message = 'Please enable curl extension. <a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_help">Click here</a> for the steps to enable curl or check Help & Troubleshooting.';
      return json_encode(array("status" => 'ERROR', "message" => $message));
    }$url = variable_get('two_factor_authentication_mo2f_host_name') . '/moas/api/auth/auth-status';
    $ch = curl_init($url);

    /* The customer Key provided to you */

    $customerKey = variable_get('two_factor_authentication_mo2f_customerKey');

    /* The customer API Key provided to you */

    $apiKey = variable_get('two_factor_authentication_mo2f_api_key');

    /* Current time in milliseconds since midnight, January 1, 1970 UTC. */

    $currentTimeInMillis = round(microtime(TRUE) * 1000);

    /* Creating the Hash using SHA-512 algorithm */

    $stringToHash = $customerKey . number_format($currentTimeInMillis, 0, '', '') . $apiKey;
    $hashValue = hash("sha512", $stringToHash);

    $customerKeyHeader = "Customer-Key: " . $customerKey;
    $timestampHeader = "Timestamp: " . $currentTimeInMillis;
    $authorizationHeader = "Authorization: " . $hashValue;

    $fields = array(
      'txId' => $tId,
    );

    $field_string = json_encode($fields);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    // Required for https urls.
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt(
          $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customerKeyHeader,
            $timestampHeader, $authorizationHeader,
          )
      );
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $content = curl_exec($ch);

    if (curl_errno($ch)) {
      return NULL;
    }

    curl_close($ch);
    return $content;
  }

  /**
   *
   */
  function register_mobile($useremail) {

    if (!MO2f_Utility::is_curl_installed()) {
      $message = 'Please enable curl extension. <a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_help">Click here</a> for the steps to enable curl or check Help & Troubleshooting.';
      return json_encode(array("status" => 'ERROR', "message" => $message));
    }

    $url = variable_get('two_factor_authentication_mo2f_host_name') . '/moas/api/auth/register-mobile';
    $ch = curl_init($url);
    $this->email = $useremail;

    /* The customer Key provided to you */

    $customerKey = variable_get('two_factor_authentication_mo2f_customerKey');

    /* The customer API Key provided to you */

    $apiKey = variable_get('two_factor_authentication_mo2f_api_key');

    /* Current time in milliseconds since midnight, January 1, 1970 UTC. */

    $currentTimeInMillis = round(microtime(TRUE) * 1000);

    /* Creating the Hash using SHA-512 algorithm */

    $stringToHash = $customerKey . number_format($currentTimeInMillis, 0, '', '') . $apiKey;
    $hashValue = hash("sha512", $stringToHash);

    $customerKeyHeader = "Customer-Key: " . $customerKey;
    $timestampHeader = "Timestamp: " . $currentTimeInMillis;
    $authorizationHeader = "Authorization: " . $hashValue;

    $fields = array(
      'username' => $this->email,
    );

    $field_string = json_encode($fields);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    // Required for https urls.
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt(
          $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customerKeyHeader,
            $timestampHeader, $authorizationHeader,
          )
      );
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $content = curl_exec($ch);

    if (curl_errno($ch)) {
      return NULL;
    }

    curl_close($ch);
    return $content;
  }

  /**
   *
   */
  function mo_check_user_already_exist($email) {

    if (!MO2f_Utility::is_curl_installed()) {
      $message = 'Please enable curl extension. <a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_help">Click here</a> for the steps to enable curl or check Help & Troubleshooting.';
      return json_encode(array("status" => 'ERROR', "message" => $message));
    }

    $url = variable_get('two_factor_authentication_mo2f_host_name') . '/moas/api/admin/users/search';
    $ch = curl_init($url);

    /* The customer Key provided to you */

    $customerKey = variable_get('two_factor_authentication_mo2f_customerKey');

    /* The customer API Key provided to you */

    $apiKey = variable_get('two_factor_authentication_mo2f_api_key');

    /* Current time in milliseconds since midnight, January 1, 1970 UTC. */

    $currentTimeInMillis = round(microtime(TRUE) * 1000);

    /* Creating the Hash using SHA-512 algorithm */

    $stringToHash = $customerKey . number_format($currentTimeInMillis, 0, '', '') . $apiKey;
    $hashValue = hash("sha512", $stringToHash);

    $customerKeyHeader = "Customer-Key: " . $customerKey;
    $timestampHeader = "Timestamp: " . $currentTimeInMillis;
    $authorizationHeader = "Authorization: " . $hashValue;

    $fields = array(
      'customerKey' => $customerKey,
      'username' => $email,
    );

    $field_string = json_encode($fields);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    // Required for https urls.
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt(
          $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customerKeyHeader,
            $timestampHeader, $authorizationHeader,
          )
      );
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $content = curl_exec($ch);

    if (curl_errno($ch)) {
      return NULL;
    }

    curl_close($ch);
    return $content;
  }

  /**
   *
   */
  function mo_create_user($currentuser, $email) {

    if (!MO2f_Utility::is_curl_installed()) {
      $message = 'Please enable curl extension. <a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_help">Click here</a> for the steps to enable curl or check Help & Troubleshooting.';
      return json_encode(array("status" => 'ERROR', "message" => $message));
    }

    $url = variable_get('two_factor_authentication_mo2f_host_name') . '/moas/api/admin/users/create';
    $ch = curl_init($url);

    /* The customer Key provided to you */

    $customerKey = variable_get('two_factor_authentication_mo2f_customerKey');

    /* The customer API Key provided to you */

    $apiKey = variable_get('two_factor_authentication_mo2f_api_key');

    /* Current time in milliseconds since midnight, January 1, 1970 UTC. */

    $currentTimeInMillis = round(microtime(TRUE) * 1000);

    /* Creating the Hash using SHA-512 algorithm */

    $stringToHash = $customerKey . number_format($currentTimeInMillis, 0, '', '') . $apiKey;
    $hashValue = hash("sha512", $stringToHash);

    $customerKeyHeader = "Customer-Key: " . $customerKey;
    $timestampHeader = "Timestamp: " . $currentTimeInMillis;
    $authorizationHeader = "Authorization: " . $hashValue;

    $fields = array(
      'customerKey' => $customerKey,
      'username' => $email,
      'firstName' => $currentuser->name,
    );
    $field_string = json_encode($fields);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    // Required for https urls.
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt(
          $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customerKeyHeader,
            $timestampHeader, $authorizationHeader,
          )
      );
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $content = curl_exec($ch);
    if (curl_errno($ch)) {
      return NULL;
    }

    curl_close($ch);
    return $content;
  }

  /**
   *
   */
  function mo2f_get_userinfo($email) {

    if (!MO2f_Utility::is_curl_installed()) {
      $message = 'Please enable curl extension. <a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_help">Click here</a> for the steps to enable curl or check Help & Troubleshooting.';
      return json_encode(array("status" => 'ERROR', "message" => $message));
    }

    $url = variable_get('two_factor_authentication_mo2f_host_name') . '/moas/api/admin/users/get';
    $ch = curl_init($url);

    /* The customer Key provided to you */

    $customerKey = variable_get('two_factor_authentication_mo2f_customerKey');

    /* The customer API Key provided to you */

    $apiKey = variable_get('two_factor_authentication_mo2f_api_key');

    /* Current time in milliseconds since midnight, January 1, 1970 UTC. */

    $currentTimeInMillis = round(microtime(TRUE) * 1000);

    /* Creating the Hash using SHA-512 algorithm */

    $stringToHash = $customerKey . number_format($currentTimeInMillis, 0, '', '') . $apiKey;
    $hashValue = hash("sha512", $stringToHash);

    $customerKeyHeader = "Customer-Key: " . $customerKey;
    $timestampHeader = "Timestamp: " . $currentTimeInMillis;
    $authorizationHeader = "Authorization: " . $hashValue;

    $fields = array(
      'customerKey' => $customerKey,
      'username' => $email,
    );

    $field_string = json_encode($fields);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    // Required for https urls.
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt(
          $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customerKeyHeader,
            $timestampHeader, $authorizationHeader,
          )
      );
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $content = curl_exec($ch);

    if (curl_errno($ch)) {
      return NULL;
    }
    curl_close($ch);
    return $content;
  }

  /**
   *
   */
  function mo2f_update_userinfo($email, $authType, $phone, $tname, $enableAdminSecondFactor) {

    if (!MO2f_Utility::is_curl_installed()) {
      $message = 'Please enable curl extension. <a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_help">Click here</a> for the steps to enable curl or check Help & Troubleshooting.';
      return json_encode(array("status" => 'ERROR', "message" => $message));
    }

    $url = variable_get('two_factor_authentication_mo2f_host_name') . '/moas/api/admin/users/update';
    $ch = curl_init($url);

    /* The customer Key provided to you */

    $customerKey = variable_get('two_factor_authentication_mo2f_customerKey');

    /* The customer API Key provided to you */

    $apiKey = variable_get('two_factor_authentication_mo2f_api_key');

    /* Current time in milliseconds since midnight, January 1, 1970 UTC. */

    $currentTimeInMillis = round(microtime(TRUE) * 1000);

    /* Creating the Hash using SHA-512 algorithm */

    $stringToHash = $customerKey . number_format($currentTimeInMillis, 0, '', '') . $apiKey;
    $hashValue = hash("sha512", $stringToHash);

    $customerKeyHeader = "Customer-Key: " . $customerKey;
    $timestampHeader = "Timestamp: " . $currentTimeInMillis;
    $authorizationHeader = "Authorization: " . $hashValue;
    if ($authType == 'PUSH') {
      $authType = 'PUSH NOTIFICATIONS';
    }

    $fields = array(
      'customerKey' => $customerKey,
      'username' => $email,
      'phone' => $phone,
      'authType' => $authType,
      'transactionName' => $tname,
      'adminLoginSecondFactor' => $enableAdminSecondFactor,
    );

    $field_string = json_encode($fields);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    // Required for https urls.
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt(
          $ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", $customerKeyHeader,
            $timestampHeader, $authorizationHeader,
          )
      );
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $content = curl_exec($ch);

    if (curl_errno($ch)) {
      return NULL;
    }
    curl_close($ch);
    return $content;
  }

}
