<?Php

/**
 * @file MiniOrange enables user to log in through mobile authentication as an additional layer of security over password.
 * Copyright (C) 2015  miniOrange.
 *  * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package miniOrange OAuth
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 */

/**
 * This library is miniOrange Authentication Service.
 * Contains Request Calls to Customer service. *  .**/
class Miniorange_User_Register{
  /**
   *
   */
  function __construct() {
    add_action('admin_init', array($this, 'miniorange_user_save_settings'));
    add_action('admin_enqueue_scripts', array($this, 'plugin_settings_style'));
  }

  /**
   *
   */
  function plugin_settings_style() {
    wp_enqueue_style('mo_2_factor_admin_settings_style', plugins_url('includes/css/style_settings.css?version=3.0', __FILE__));
  }

  /**
   *
   */
  function mo_auth_success_message() {
    $message = get_option('mo2f_message');
    ?>
		<script>

		jQuery(document).ready(function() {

		 var message = "<?php echo $message; ?>";
		jQuery('#messages').append("<div class='error notice is-dismissible mo2f_error_container'> <p class='mo2f_msgs'>" + message + "</p></div>");
		});
		</script>
		<?php
  }

  /**
   *
   */
  function mo_auth_error_message() {
    $message = get_option('mo2f_message');
    ?>
		<script>
		jQuery(document).ready(function() {

			var message = "<?php echo $message; ?>";
			jQuery('#messages').append("<div class='updated notice is-dismissible mo2f_success_container'> <p class='mo2f_msgs'>" + message + "</p></div>");

			jQuery('a[href=#test]').click(function() {
				var currentMethod = jQuery(this).data("method");

				if(currentMethod == 'MOBILE AUTHENTICATION'){
					jQuery('#mo2f_2factor_test_mobile_form').submit();
				}else if(currentMethod == 'PUSH NOTIFICATIONS'){
					jQuery('#mo2f_2factor_test_push_form').submit();
				}else if(currentMethod == 'SOFT TOKEN'){
					jQuery('#mo2f_2factor_test_softtoken_form').submit();
				}else if(currentMethod == 'SMS' || currentMethod == 'PHONE VERIFICATION'){
					jQuery('#mo2f_test_2factor_method').val(currentMethod);
					jQuery('#mo2f_2factor_test_smsotp_form').submit();
				}else if(currentMethod == 'OUT OF BAND EMAIL'){
					jQuery('#mo2f_2factor_test_out_of_band_email_form').submit();
				}else if(currentMethod == 'GOOGLE AUTHENTICATOR'){
					jQuery('#mo2f_2factor_test_google_auth_form').submit();
				}
			});

		});
		</script>
		<?php
  }/**
    *
    */
  private function mo_auth_show_success_message() {
    remove_action('admin_notices', array($this, 'mo_auth_success_message'));
    add_action('admin_notices', array($this, 'mo_auth_error_message'));
  }

  /**
   *
   */
  private function mo_auth_show_error_message() {
    remove_action('admin_notices', array($this, 'mo_auth_error_message'));
    add_action('admin_notices', array($this, 'mo_auth_success_message'));
  }

  /**
   *
   */
  public function mo2f_register_user() {
    global $wpdb;
    global $current_user;
    get_currentuserinfo();
    if (mo_2factor_is_curl_installed() == 0) {
      ?>
			<p style="color:red;">(Warning: <a href="http://php.net/manual/en/curl.installation.php" target="_blank">PHP CURL extension</a> is not installed or disabled)</p>
		<?php
    }
    $mo2f_active_tab = isset($_GET['mo2f_tab']) ? $_GET['mo2f_tab'] : '2factor_setup';

    ?>
		<div id="tab">
			<h2 class="nav-tab-wrapper">
				<a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=2factor_setup" class="nav-tab <?php echo $mo2f_active_tab == '2factor_setup' ? 'nav-tab-active' : ''; ?>" id="mo2f_tab1"><?php if (get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_INITIALIZE_MOBILE_REGISTRATION' || get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS') {
?>User Profile <?php
}
else {
      ?> Account Setup <?php
} ?></a>
				<a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mobile_configure" class="nav-tab <?php echo $mo2f_active_tab == 'mobile_configure' ? 'nav-tab-active' : ''; ?>" id="mo2f_tab2">Setup Two-Factor</a>

				<?php if (get_option('mo2f_deviceid_enabled')) {
?>
    //echo $mo2f_active_tab == 'advance_option' ? 'nav-tab-active' : '';
				<!-- <a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=advance_option" class="nav-tab <?php
  ?>" id="mo2f_tab5">Device Management</a> -->
				<?php
} ?>
				<a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_demo" class="nav-tab <?php echo $mo2f_active_tab == 'mo2f_demo' ? 'nav-tab-active' : ''; ?>" id="mo2f_tab4">How It Works</a>
				<a href="admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mo2f_help" class="nav-tab <?php echo $mo2f_active_tab == 'mo2f_help' ? 'nav-tab-active' : ''; ?>" id="mo2f_tab3">Help & Troubleshooting</a>
			</h2>
		</div>

		<div class="mo2f_container">
		<div id="messages"></div>
			<table style="width:100%;">
				<tr>
					<td style="width:60%;vertical-align:top;">
		<?php
    if ($mo2f_active_tab == 'mobile_configure') {
    $mo2f_second_factor = mo2f_get_activated_second_factor($current_user);
    ?>
			<script>
				jQuery(document).ready(function(){
					jQuery("#mo2f_support_table").hide();
				});
			</script>
    <?php
    mo2f_select_2_factor_method($current_user, $mo2f_second_factor);
    }
    elseif ($mo2f_active_tab == 'mo2f_demo') {
      show_2_factor_login_demo($current_user);
    }
    elseif ($mo2f_active_tab == 'mo2f_help') {
      mo2f_show_help_and_troubleshooting($current_user);
    }
    elseif (get_option('mo2f_deviceid_enabled') && $mo2f_active_tab == 'advance_option') {
      // Login Settings tab.
      show_2_factor_advanced_options($current_user);
    }
    else {
      if (get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_SUCCESS' || get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_FAILURE') {
        mo2f_show_user_otp_validation_page();
      }
      elseif (get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_INITIALIZE_MOBILE_REGISTRATION') {
        $mo2f_second_factor = mo2f_get_activated_second_factor($current_user);
        mo2f_show_instruction_to_allusers($current_user, $mo2f_second_factor);
      }
      elseif (get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS') {
        $mo2f_second_factor = mo2f_get_activated_second_factor($current_user);
        mo2f_show_instruction_to_allusers($current_user, $mo2f_second_factor);
      }
      else {
        show_user_welcome_page($current_user);
      }
    }
    ?>
					</td>
					<td style="vertical-align:top;padding-left:1%;" id="mo2f_support_table">
					</td>
				</tr>
			</table>
		</div>
		<?php
  }

  /**
   *
   */
  function miniorange_user_save_settings() {
    global $wpdb;
    global $current_user;
    get_currentuserinfo();

    if (isset($_POST['miniorange_get_started']) && isset($_POST['miniorange_user_reg_nonce'])) {
      $nonce = $_POST['miniorange_user_reg_nonce'];
      if (!wp_verify_nonce($nonce, 'miniorange-2-factor-user-reg-nonce')) {
        update_option('mo2f_message', 'Invalid request');
      }
      else {
        $email = '';
        if (MO2f_Utility::mo2f_check_empty_or_null($_POST['mo_useremail'])) {
          update_option('mo2f_message', 'Please enter email-id to register.');
          return;
        }
        else {
          $email = sanitize_email($_POST['mo_useremail']);
        }

        if (!MO2f_Utility::check_if_email_is_already_registered($email)) {
          update_user_meta($current_user->ID, 'mo_2factor_user_email', $email);
          $enduser = new Customer_Setup();
          $content = json_decode($enduser->send_otp_token($email, 'EMAIL', get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key')), TRUE);
          if (strcasecmp($content['status'], 'SUCCESS') == 0) {
            update_option('mo2f_message', 'An OTP has been sent to <b>' . ($email) . '</b>. Please enter the OTP below to verify your email. ');
            $_SESSION['mo2f_transactionId'] = $content['txId'];
            update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_SUCCESS');
            $this->mo_auth_show_success_message();
          }
          else {
            update_option('mo2f_message', 'There was an error in sending OTP over email. Please click on Resend OTP to try again.');
            update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_FAILURE');
            $this->mo_auth_show_error_message();
          }
        }
        else {
          update_option('mo2f_message', 'The email is already used by other user. Please register with other email.');
          $this->mo_auth_show_error_message();
        }
      }
    }

    // Validate OTP.
    if (isset($_POST['option']) and $_POST['option'] == "mo_2factor_validate_user_otp") {

      // Validation and sanitization.
      $otp_token = '';
      if (MO2f_Utility::mo2f_check_empty_or_null($_POST['otp_token'])) {
        update_option('mo2f_message', 'All the fields are required. Please enter valid entries.');
        $this->mo_auth_show_error_message();
        return;
      }
      else {
        $otp_token = sanitize_text_field($_POST['otp_token']);
      }

      if (!MO2f_Utility::check_if_email_is_already_registered(get_user_meta($current_user->ID, 'mo_2factor_user_email', TRUE))) {
        $customer = new Customer_Setup();
        $content = json_decode($customer->validate_otp_token('EMAIL', NULL, $_SESSION['mo2f_transactionId'], $otp_token), TRUE);
        if ($content['status'] == 'ERROR') {
          update_option('mo2f_message', $content['message']);
        }
        else {
          // OTP validated and generate QRCode.
          if (strcasecmp($content['status'], 'SUCCESS') == 0) {
            $this->mo2f_create_user($current_user, get_user_meta($current_user->ID, 'mo_2factor_user_email', TRUE));
            // OTP Validation failed.
          }
          else {
            update_option('mo2f_message', 'Invalid OTP. Please try again.');
            update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_FAILURE');
            $this->mo_auth_show_error_message();
          }
        }
      }
      else {
        update_option('mo2f_message', 'The email is already used by other user. Please register with other email by clicking on Back button.');
        $this->mo_auth_show_error_message();
      }
    }

    // Resend OTP.
    if (isset($_POST['option']) and trim($_POST['option']) == "mo_2factor_resend_user_otp") {
      $customer = new Customer_Setup();
      $content = json_decode($customer->send_otp_token(get_user_meta($current_user->ID, 'mo_2factor_user_email', TRUE), 'EMAIL', get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key')), TRUE);
      if (strcasecmp($content['status'], 'SUCCESS') == 0) {
        update_option('mo2f_message', 'An OTP has been sent to <b>' . (get_user_meta($current_user->ID, 'mo_2factor_user_email', TRUE)) . '</b>. Please enter the OTP below to verify your email. ');
        $_SESSION['mo2f_transactionId'] = $content['txId'];
        update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_SUCCESS');
        $this->mo_auth_show_success_message();
      }
      else {
        update_option('mo2f_message', 'There was an error in sending email. Please click on Resend OTP to try again.');
        update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_FAILURE');
        $this->mo_auth_show_error_message();
      }
    }

    // Back to registration page for additional admin.
    if (isset($_POST['option']) and $_POST['option'] == 'mo_2factor_backto_user_registration') {
      delete_user_meta($current_user->ID, 'mo_2factor_user_email');
      unset($_SESSION['mo2f_transactionId']);
      delete_user_meta($current_user->ID, 'mo_2factor_user_registration_status');
      delete_user_meta($current_user->ID, 'mo_2factor_map_id_with_email');
    }
    // Mobile registration.
    if (isset($_POST['option']) and ($_POST['option'] == "mo_auth_mobile_registration_complete" || $_POST['option'] == 'mo_auth_mobile_reconfiguration_complete')) {
      unset($_SESSION['mo2f_qrCode']);
      unset($_SESSION['mo2f_transactionId']);
      unset($_SESSION['mo2f_show_qr_code']);
      $email = get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE);
      $enduser = new Two_Factor_Setup();
      $response = json_decode($enduser->mo2f_update_userinfo($email, get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE), NULL, NULL, NULL), TRUE);
      if (json_last_error() == JSON_ERROR_NONE) {
        /* Generate Qr code */

        if ($response['status'] == 'ERROR') {
          update_option('mo2f_message', $response['message']);
          $this->mo_auth_show_error_message();
        }
        elseif ($response['status'] == 'SUCCESS') {
          $selectedMethod = get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE);
          $testmethod = $selectedMethod;
          if ($selectedMethod == 'MOBILE AUTHENTICATION') {
            $selectedMethod = "QR Code Authentication";
          }
          $message = '<b>' . $selectedMethod . '</b> is set as your 2nd factor method. <a href=\"#test\" data-method=\"' . $testmethod . '\">Click Here</a> to test ' . $selectedMethod . ' method.';
          update_option('mo2f_message', $message);

          update_user_meta($current_user->ID, 'mo2f_mobile_registration_status', TRUE);
          delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
          update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS');
          delete_user_meta($current_user->ID, 'mo_2factor_mobile_registration_status');
          $this->mo_auth_show_success_message();
        }
        else {
          update_option('mo2f_message', 'An error occured while processing your request. Please Try again.');
          $this->mo_auth_show_error_message();
        }
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please try again');
        $this->mo_auth_show_error_message();
      }
    }
    // Mobile registration.
    if (isset($_POST['option']) and $_POST['option'] == 'mo2f_mobile_authenticate_success') {
      update_option('mo2f_message', 'You have successfully completed the test. <a href=' . wp_login_url() . '?action=logout><b>Click Here</b></a> to logout and try login with 2-Factor.');
      delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
      unset($_SESSION['mo2f_qrCode']);
      unset($_SESSION['mo2f_transactionId']);
      unset($_SESSION['mo2f_show_qr_code']);
      $this->mo_auth_show_success_message();
    }

    // Mobile registration.
    if (isset($_POST['option']) and $_POST['option'] == 'mo2f_mobile_authenticate_error') {
      update_option('mo2f_message', 'Authentication failed. Please try again to test the configuration.');
      unset($_SESSION['mo2f_show_qr_code']);
      $this->mo_auth_show_error_message();
    }

    if (isset($_POST['option']) && $_POST['option'] == 'mo_2factor_test_mobile_authentication') {

      $challengeMobile = new Customer_Setup();
      $content = $challengeMobile->send_otp_token(get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE), 'MOBILE AUTHENTICATION', get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key'));
      $response = json_decode($content, TRUE);
      if (json_last_error() == JSON_ERROR_NONE) {
        /* Generate Qr code */

        if ($response['status'] == 'ERROR') {
          update_option('mo2f_message', $response['message']);
        }
        else {
          $_SESSION['mo2f_qrCode'] = $response['qrCode'];
          $_SESSION['mo2f_transactionId'] = $response['txId'];
          $_SESSION['mo2f_show_qr_code'] = 'MO_2_FACTOR_SHOW_QR_CODE';
          update_user_meta($current_user->ID, 'mo2f_configure_test_option', 'MO2F_TEST');
          update_user_meta($current_user->ID, 'mo2f_selected_2factor_method', 'MOBILE AUTHENTICATION');
          update_option('mo2f_message', 'Please scan the QR Code now.');
          $this->mo_auth_show_success_message();
        }
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please try again');
        $this->mo_auth_show_error_message();
      }
    }

    // Redirect to setings page.
    if (isset($_POST['option']) and $_POST['option'] == "mo_auth_setting_configuration") {
      update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS');
    }
    // Refrsh Qrcode.
    if (isset($_POST['option']) and $_POST['option'] == "mo_auth_refresh_mobile_qrcode") {
      if (get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_INITIALIZE_MOBILE_REGISTRATION' || get_user_meta($current_user->ID, 'mo_2factor_user_registration_status', TRUE) == 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS') {
        $email = get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE);
        $this->mo2f_get_qr_code_for_mobile($email, $current_user->ID);
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please register with miniOrange before configuring your mobile.');
        $this->mo_auth_show_error_message();
      }
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo_2factor_test_soft_token') {
      update_user_meta($current_user->ID, 'mo2f_configure_test_option', 'MO2F_TEST');
      update_user_meta($current_user->ID, 'mo2f_selected_2factor_method', 'SOFT TOKEN');
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_validate_soft_token') {
      $otp_token = '';
      if (MO2f_Utility::mo2f_check_empty_or_null($_POST['otp_token'])) {
        update_option('mo2f_message', 'Please enter a value to test your authentication.');
        $this->mo_auth_show_error_message();
        return;
      }
      else {
        $otp_token = sanitize_text_field($_POST['otp_token']);
      }
      $email = get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE);
      $customer = new Customer_Setup();
      $content = json_decode($customer->validate_otp_token('SOFT TOKEN', $email, NULL, $otp_token), TRUE);
      if ($content['status'] == 'ERROR') {
        update_option('mo2f_message', $content['message']);
        $this->mo_auth_show_error_message();
      }
      else {
        // OTP validated and generate QRCode.
        if (strcasecmp($content['status'], 'SUCCESS') == 0) {
          update_option('mo2f_message', 'You have successfully completed the test. <a href=' . wp_login_url() . '?action=logout><b>Click Here</b></a> to logout and try login with 2-Factor.');
          delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
          $this->mo_auth_show_success_message();

          // OTP Validation failed.
        }
        else {
          update_option('mo2f_message', 'Invalid OTP. Please try again.');
          $this->mo_auth_show_error_message();
        }
      }
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo_2factor_test_otp_over_sms') {
      update_user_meta($current_user->ID, 'mo2f_configure_test_option', 'MO2F_TEST');
      update_user_meta($current_user->ID, 'mo2f_selected_2factor_method', $_POST['mo2f_selected_2factor_method']);
      $email = get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE);
      $phone = get_user_meta($current_user->ID, 'mo2f_user_phone', TRUE);
      $enduser = new Customer_Setup();
      $content = json_decode($enduser->send_otp_token($email, $_POST['mo2f_selected_2factor_method'], get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key')), TRUE);
      if (strcasecmp($content['status'], 'SUCCESS') == 0) {
        if (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'SMS') {
          update_option('mo2f_message', 'An OTP has been sent to <b>' . ($phone) . '</b>. Please enter the one time passcode below. ');
        }
        elseif (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'PHONE VERIFICATION') {
          update_option('mo2f_message', 'You will receive a phone call on this number ' . $phone . '. Please enter the one time passcode below.');
        }

        $_SESSION['mo2f_transactionId'] = $content['txId'];
        $this->mo_auth_show_success_message();
      }
      else {
        update_option('mo2f_message', 'There was an error in sending one time passcode. Please click on Resend OTP to try again.');
        $this->mo_auth_show_error_message();
      }
    }

    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_validate_otp_over_sms') {
      $otp_token = '';
      if (MO2f_Utility::mo2f_check_empty_or_null($_POST['otp_token'])) {
        update_option('mo2f_message', 'Please enter a value to test your authentication.');
        $this->mo_auth_show_error_message();
        return;
      }
      else {
        $otp_token = sanitize_text_field($_POST['otp_token']);
      }
      $email = get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE);
      $customer = new Customer_Setup();
      $content = json_decode($customer->validate_otp_token('SMS', $email, $_SESSION['mo2f_transactionId'], $otp_token), TRUE);
      if ($content['status'] == 'ERROR') {
        update_option('mo2f_message', $content['message']);
        $this->mo_auth_show_error_message();
      }
      else {
        // OTP validated and generate QRCode.
        if (strcasecmp($content['status'], 'SUCCESS') == 0) {
          update_option('mo2f_message', 'You have successfully completed the test. <a href=' . wp_login_url() . '?action=logout><b>Click Here</b></a> to logout and try login with 2-Factor.');
          delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
          $this->mo_auth_show_success_message();

          // OTP Validation failed.
        }
        else {
          update_option('mo2f_message', 'Invalid OTP. Please try again.');
          $this->mo_auth_show_error_message();
        }
      }
    }

    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_save_2factor_method') {
      if (get_user_meta($current_user->ID, 'mo_2factor_user_registration_with_miniorange', TRUE) == 'SUCCESS') {
        // Status for configuring the specific 2nd-factor method.
        update_user_meta($current_user->ID, 'mo2f_configure_test_option', 'MO2F_CONFIGURE');
        // Status for second factor selected by user.
        update_user_meta($current_user->ID, 'mo2f_selected_2factor_method', $_POST['mo2f_selected_2factor_method']);
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please register with miniOrange to configure 2 Factor plugin.');
        $this->mo_auth_show_error_message();
      }
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_update_2factor_method') {
      $email = get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE);
      $enduser = new Two_Factor_Setup();
      update_user_meta($current_user->ID, 'mo2f_selected_2factor_method', $_POST['mo2f_selected_2factor_method']);

      $response = json_decode($enduser->mo2f_update_userinfo($email, $_POST['mo2f_selected_2factor_method'], NULL, NULL, NULL), TRUE);

      if (json_last_error() == JSON_ERROR_NONE) {
        if ($response['status'] == 'ERROR') {
          update_option('mo2f_message', $response['message']);
          $this->mo_auth_show_error_message();
        }
        elseif ($response['status'] == 'SUCCESS') {
          $selectedMethod = get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE);
          if ($selectedMethod == 'OUT OF BAND EMAIL') {
            $selectedMethod = "Email Verification";
            update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS');
          }
          elseif ($selectedMethod == 'MOBILE AUTHENTICATION') {
            $selectedMethod = "QR Code Authentication";
          }
          elseif (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'SMS') {
            $authType = "OTP Over SMS";
          }
          delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
          delete_user_meta($current_user->ID, 'mo_2factor_mobile_registration_status');
          update_option('mo2f_message', $selectedMethod . ' is set as your Two-Factor method.');
          $this->mo_auth_show_success_message();
        }
        else {
          update_option('mo2f_message', 'An error occured while processing your request. Please Try again.');
          $this->mo_auth_show_error_message();
        }
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please try again');
        $this->mo_auth_show_error_message();
      }
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_verify_phone') {
      $phone = sanitize_text_field($_POST['verify_phone']);

      if (MO2f_Utility::mo2f_check_empty_or_null($phone)) {
        update_option('mo2f_message', 'All the fields are required. Please enter valid entries.');
        $this->mo_auth_show_error_message();
        return;
      }
      $phone = str_replace(' ', '', $phone);
      $_SESSION['two_factor_authentication_mo2f_phone'] = $phone;

      $customer = new Customer_Setup();

      if (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'SMS') {
        $currentMethod = "OTP_OVER_SMS";
      }
      elseif (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'PHONE VERIFICATION') {
        $currentMethod = "PHONE_VERIFICATION";
      }

      $content = json_decode($customer->send_otp_token($phone, $currentMethod, get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key')), TRUE);

      if (json_last_error() == JSON_ERROR_NONE) {
        /* Generate otp token */

        if ($content['status'] == 'ERROR') {
          update_option('mo2f_message', $response['message']);
          $this->mo_auth_show_error_message();
        }
        elseif ($content['status'] == 'SUCCESS') {
          $_SESSION['mo2f_transactionId'] = $content['txId'];

          if (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'SMS') {
            update_option('mo2f_message', 'The One Time Passcode has been sent to ' . $phone . '. Please enter the one time passcode below to verify your number.');
          }
          elseif (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'PHONE VERIFICATION') {
            update_option('mo2f_message', 'You will receive a phone call on this number ' . $phone . '. Please enter the one time passcode below to verify your number.');
          }
          if (get_user_meta($current_user->ID, 'mo2f_user_phone', TRUE) && strlen(get_user_meta($current_user->ID, 'mo2f_user_phone', TRUE)) >= 4) {
            if ($_SESSION['two_factor_authentication_mo2f_phone'] != get_user_meta($current_user->ID, 'mo2f_user_phone', TRUE)) {
              if (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'SMS') {
                $message = 'The One Time Passcode has been sent to ' . $phone . '. Please enter the one time passcode below to verify your number. <br /><div style=\"color:red;\" ><b>Alert: </b>You are trying to configure OTP over SMS on new phone number.So, You need to reconfigure your mobile app again.</div>';
                update_option('mo2f_message', $message);
              }
              elseif (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'PHONE VERIFICATION') {
                $message = 'You will receive a phone call on this number ' . $phone . '. Please enter the one time passcode below to verify your number. <br /><div style=\"color:red;\" ><b>Alert: </b>You are trying to configure Phone Call Verification on new phone number.So, You need to reconfigure your mobile app again.</div>';
                update_option('mo2f_message', $message);
              }
            }
          }
          $this->mo_auth_show_success_message();
        }
        else {
          update_option('mo2f_message', 'An error occured while processing your request. Please Try again.');
          $this->mo_auth_show_error_message();
        }
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please try again');
        $this->mo_auth_show_error_message();
      }
    }

    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_validate_otp') {
      $otp_token = '';
      if (MO2f_Utility::mo2f_check_empty_or_null($_POST['otp_token'])) {
        update_option('mo2f_message', 'All the fields are required. Please enter valid entries.');
        $this->mo_auth_show_error_message();
        return;
      }
      else {
        $otp_token = sanitize_text_field($_POST['otp_token']);
      }

      $customer = new Customer_Setup();
      $content = json_decode($customer->validate_otp_token(get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE), NULL, $_SESSION['mo2f_transactionId'], $otp_token), TRUE);
      if ($content['status'] == 'ERROR') {
        update_option('mo2f_message', $content['message']);

        // OTP validated.
      }
      elseif (strcasecmp($content['status'], 'SUCCESS') == 0) {
        if (get_user_meta($current_user->ID, 'mo2f_user_phone', TRUE) && strlen(get_user_meta($current_user->ID, 'mo2f_user_phone', TRUE)) >= 4) {
          if ($_SESSION['two_factor_authentication_mo2f_phone'] != get_user_meta($current_user->ID, 'mo2f_user_phone', TRUE)) {
            update_user_meta($current_user->ID, 'mo2f_mobile_registration_status', FALSE);
          }
        }
        $email = get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE);
        $phone = $_SESSION['two_factor_authentication_mo2f_phone'];

        $enduser = new Two_Factor_Setup();
        $response = json_decode($enduser->mo2f_update_userinfo($email, get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE), $phone, NULL, NULL), TRUE);
        if (json_last_error() == JSON_ERROR_NONE) {

          if ($response['status'] == 'ERROR') {
            unset($_SESSION['two_factor_authentication_mo2f_phone']);
            update_option('mo2f_message', $response['message']);
            $this->mo_auth_show_error_message();
          }
          elseif ($response['status'] == 'SUCCESS') {
            delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
            update_user_meta($current_user->ID, 'mo2f_otp_registration_status', TRUE);
            delete_user_meta($current_user->ID, 'mo_2factor_mobile_registration_status');
            update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS');
            update_user_meta($current_user->ID, 'mo2f_user_phone', $_SESSION['two_factor_authentication_mo2f_phone']);
            unset($_SESSION['two_factor_authentication_mo2f_phone']);
            $testmethod = get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE);
            if (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'SMS') {
              $authType = "OTP Over SMS";
            }
            elseif (get_user_meta($current_user->ID, 'mo2f_selected_2factor_method', TRUE) == 'PHONE VERIFICATION') {
              $authType = "Phone Call Verification";
            }
            $message = '<b>' . $authType . '</b> is set as your 2nd factor method. <a href=\"#test\" data-method=\"' . $testmethod . '\">Click Here</a> to test ' . $authType . ' method.';
            update_option('mo2f_message', $message);
            $this->mo_auth_show_success_message();
          }
          else {
            unset($_SESSION['two_factor_authentication_mo2f_phone']);
            update_option('mo2f_message', 'An error occured while processing your request. Please Try again.');
            $this->mo_auth_show_error_message();
          }
        }
        else {
          unset($_SESSION['two_factor_authentication_mo2f_phone']);
          update_option('mo2f_message', 'Invalid request. Please try again');
          $this->mo_auth_show_error_message();
        }

        // OTP Validation failed.
      }
      else {
        update_option('mo2f_message', 'Invalid OTP. Please try again.');
        $this->mo_auth_show_error_message();
      }
    }

    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_cancel_configuration') {
      unset($_SESSION['mo2f_qrCode']);
      unset($_SESSION['mo2f_transactionId']);
      unset($_SESSION['mo2f_show_qr_code']);
      unset($_SESSION['two_factor_authentication_mo2f_phone']);
      delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
    }

    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_push_success') {
      update_option('mo2f_message', 'You have successfully completed the test. <a href=' . wp_login_url() . '?action=logout><b>Click Here</b></a> to logout and try login with 2-Factor.');
      update_user_meta($current_user->ID, 'mo2f_configure_test_option', 'MO2F_TEST');
      $this->mo_auth_show_success_message();
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo_2factor_test_push_notification') {

      $challengeMobile = new Customer_Setup();
      $content = $challengeMobile->send_otp_token(get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE), 'PUSH NOTIFICATIONS', get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key'));
      $response = json_decode($content, TRUE);
      if (json_last_error() == JSON_ERROR_NONE) {
        /* Generate Qr code */

        if ($response['status'] == 'ERROR') {
          update_option('mo2f_message', $response['message']);
          $this->mo_auth_show_error_message();
        }
        else {
          if ($response['status'] == 'SUCCESS') {
            $_SESSION['mo2f_qrCode'] = $response['qrCode'];
            $_SESSION['mo2f_transactionId'] = $response['txId'];
            $_SESSION['mo2f_show_qr_code'] = 'MO_2_FACTOR_SHOW_QR_CODE';
            update_option('mo2f_message', 'A Push notification is sent to your registered mobile number.');
            update_user_meta($current_user->ID, 'mo2f_configure_test_option', 'MO2F_TEST');
            update_user_meta($current_user->ID, 'mo2f_selected_2factor_method', 'PUSH NOTIFICATIONS');
            $this->mo_auth_show_success_message();
          }
          else {
            unset($_SESSION['mo2f_qrCode']);
            unset($_SESSION['mo2f_transactionId']);
            unset($_SESSION['mo2f_show_qr_code']);
            update_option('mo2f_message', 'An error occured while processing your request. Please Try again.');
            $this->mo_auth_show_error_message();
          }
        }
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please try again');
        $this->mo_auth_show_error_message();
      }
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo_2factor_test_out_of_band_email') {

      $challengeMobile = new Customer_Setup();
      $content = $challengeMobile->send_otp_token(get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE), 'OUT OF BAND EMAIL', get_option('two_factor_authentication_mo2f_customerKey'), get_option('two_factor_authentication_mo2f_api_key'));
      $response = json_decode($content, TRUE);
      if (json_last_error() == JSON_ERROR_NONE) {
        /* Generate out of band email */

        if ($response['status'] == 'ERROR') {
          update_option('mo2f_message', $response['message']);
          $this->mo_auth_show_error_message();
        }
        else {
          if ($response['status'] == 'SUCCESS') {

            $_SESSION['mo2f_transactionId'] = $response['txId'];
            update_option('mo2f_message', 'An verification email is sent to<b> ' . get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE) . '</b>. Please Click on accept link to verify the email.');
            update_user_meta($current_user->ID, 'mo2f_configure_test_option', 'MO2F_TEST');
            update_user_meta($current_user->ID, 'mo2f_selected_2factor_method', 'OUT OF BAND EMAIL');
            $this->mo_auth_show_success_message();
          }
          else {
            unset($_SESSION['mo2f_transactionId']);
            update_option('mo2f_message', 'An error occured while processing your request. Please Try again.');
            $this->mo_auth_show_error_message();
          }
        }
      }
      else {
        update_option('mo2f_message', 'Invalid request. Please try again');
        $this->mo_auth_show_error_message();
      }
    }
    if (isset($_POST['option']) && $_POST['option'] == 'mo2f_out_of_band_success') {
      update_option('mo2f_message', 'You have successfully completed the test. <a href=\"' . wp_login_url() . '?action=logout\"><b>Click Here</b></a> to logout and try login with 2-Factor.');
      delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
      $this->mo_auth_show_success_message();
    }

    // Push and out of band email denied.
    if (isset($_POST['option']) and $_POST['option'] == 'mo2f_out_of_band_error') {
      update_option('mo2f_message', 'You have denied the request.');
      delete_user_meta($current_user->ID, 'mo2f_configure_test_option');
      $this->mo_auth_show_error_message();
    }
  }

  /**
   *
   */
  function mo2f_get_qr_code_for_mobile($email, $id) {
    $enduser = new Two_Factor_Setup();
    $response = json_decode($enduser->register_mobile($email), TRUE);
    if (json_last_error() == JSON_ERROR_NONE) {
      if ($response['status'] == 'ERROR') {
        update_option('mo2f_message', $response['message']);
        unset($_SESSION['mo2f_qrCode']);
        unset($_SESSION['mo2f_transactionId']);
        unset($_SESSION['mo2f_show_qr_code']);
      }
      else {
        if ($response['status'] == 'IN_PROGRESS') {
          update_option('mo2f_message', 'Please scan the QR Code now.');
          $_SESSION['mo2f_qrCode'] = $response['qrCode'];
          $_SESSION['mo2f_transactionId'] = $response['txId'];
          $_SESSION['mo2f_show_qr_code'] = 'MO_2_FACTOR_SHOW_QR_CODE';
        }
        else {
          update_option('mo2f_message', "An error occured while processing your request. Please Try again.");
          unset($_SESSION['mo2f_qrCode']);
          unset($_SESSION['mo2f_transactionId']);
          unset($_SESSION['mo2f_show_qr_code']);
        }
      }
    }
  }

  /**
   *
   */
  function mo2f_create_user($current_user, $email) {
    $enduser = new Two_Factor_Setup();
    $check_user = json_decode($enduser->mo_check_user_already_exist($email), TRUE);
    if (json_last_error() == JSON_ERROR_NONE) {
      if ($check_user['status'] == 'ERROR') {
        update_option('mo2f_message', $check_user['message']);
        $this->mo_auth_show_error_message();
      }
      else {
        if (strcasecmp($check_user['status'], 'USER_FOUND') == 0) {
          delete_user_meta($current_user->ID, 'mo_2factor_user_email');
          update_user_meta($current_user->ID, 'mo_2factor_user_registration_with_miniorange', 'SUCCESS');
          update_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', $email);
          update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS');
          $enduser->mo2f_update_userinfo(get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE), 'OUT OF BAND EMAIL', NULL, NULL, NULL);
          $message = 'You are registered successfully. <b>Email Verification</b> has been set as your default 2nd factor method. <a href=\"admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mobile_configure\" >Click Here </a>to configure another 2nd factor authentication method.';
          update_option('mo2f_message', $message);
          $this->mo_auth_show_success_message();
        }
        elseif (strcasecmp($check_user['status'], 'USER_NOT_FOUND') == 0) {
          $content = json_decode($enduser->mo_create_user($current_user, $email), TRUE);
          if (json_last_error() == JSON_ERROR_NONE) {
            if ($content['status'] == 'ERROR') {
              update_option('mo2f_message', $content['message']);
            }
            else {
              if (strcasecmp($content['status'], 'SUCCESS') == 0) {
                delete_user_meta($current_user->ID, 'mo_2factor_user_email');
                update_user_meta($current_user->ID, 'mo_2factor_user_registration_with_miniorange', 'SUCCESS');
                update_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', $email);
                update_user_meta($current_user->ID, 'mo_2factor_user_registration_status', 'TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS');
                $enduser->mo2f_update_userinfo(get_user_meta($current_user->ID, 'mo_2factor_map_id_with_email', TRUE), 'OUT OF BAND EMAIL', NULL, NULL, NULL);
                $message = 'You are registered successfully. <b>Email Verification</b> has been set as your default 2nd factor method. <a href=\"admin.php?page=miniOrange_2_factor_settings&amp;mo2f_tab=mobile_configure\" >Click Here </a>to configure another 2nd factor authentication method.';
                update_option('mo2f_message', $message);
                $this->mo_auth_show_success_message();
              }
              else {
                update_option('mo2f_message', 'Error occurred while registering the user. Please try again.');
                $this->mo_auth_show_error_message();
              }
            }
          }
          else {
            update_option('mo2f_message', 'Error occurred while registering the user. Please try again or contact your admin.');
            $this->mo_auth_show_error_message();
          }
        }
        else {
          update_option('mo2f_message', 'Error occurred while registering the user. Please try again.');
          $this->mo_auth_show_error_message();
        }
      }
    }
    else {
      update_option('mo2f_message', 'Error occurred while registering the user. Please try again.');
      $this->mo_auth_show_error_message();
    }
  }

}
