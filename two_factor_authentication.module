<?php

/**
 * @file
 * Hook_menu , hook_permission and alter for the module.
 */

define('TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_INITIALIZE_MOBILE_REGISTRATION', 1);
define('TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS', 2);
define('TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_VERIFY_CUSTOMER', 3);
define('TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_SUCCESS', 4);
define('TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_OTP_DELIVERED_FAILURE', 5);
define('TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION', 1);
define('TWO_FACTOR_AUTHENTICATION_OTP_OVER_SMS', 2);
define('TWO_FACTOR_AUTHENTICATION_PHONE_VERIFICATION', 3);
define('TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN', 4);
define('TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION', 5);
define('TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION', 6);
define('TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR', 7);
define('TFA_FACTOR_AUTHENTICATION_LOGIN_EXCLUDE', 'user/*/tfa/authenticate');

/**
 * Implements hook_help().
 */
function two_factor_authentication_help($path, $arg) {
  switch ($path) {
    case "admin/help#two_factor_authentication":
      return t('TFA login is 2 factor authenctication module used to
   introduce second factor of authentication to default
   drupal login with help of miniorange auth API');
  }
}

/**
 * Implements hook_menu().
 */
function two_factor_authentication_menu() {

  $items['admin/config/mo2f'] = array(
    'title' => 'MiniOrange',
    'description' => 'Configure two factor authentication',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/config/mo2f/mo2f_tfa'] = array(
    'title' => 'MiniOrange Settings',
    'page callback' => 'user_account_setup',
    'page arguments' => array('tfa_user_profile', 1),
    'access callback' => 'tfa_permission_for_authentic_user',
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/mo2f/mo2f_tfa/user-profile'] = array(
    'title' => 'User Profile',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['user/tfa/set-session'] = array(
    'title' => 'Setting session',
    'page callback' => 'set_session',
    'access callback' => 'tfa_permission_for_authentic_user',
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/mo2f/mo2f_tfa/setup-two-factor'] = array(
    'title' => 'Setup Two-Factor',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('two_factor_authentication_form'),
    'access callback' => 'tfa_permission_for_authentic_user',
    'access arguments' => array('access content'),
    'file' => 'setup_two_factor.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  $items['admin/config/mo2f/mo2f_tfa/setup-two-factor/user/%user/tfa/configure-mobile'] = array(
    'title' => 'Configure mobile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tfa_user_configure_mobile'),
    'access callback' => 'tfa_permission_for_authentic_user',
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/mo2f/mo2f_tfa/setup-two-factor/user/%user/tfa/google-auth'] = array(
    'title' => 'Google Authenticator',
    'page callback' => 'google_auth',
    'access callback' => 'tfa_permission_for_authentic_user',
    'file' => 'google_auth_setup.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/mo2f/mo2f_tfa/setup-two-factor/user/%user/tfa/email-verification'] = array(
    'title' => 'Email Verification',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('email_verification'),
    'access callback' => 'tfa_permission_for_authentic_user',
    'file' => 'email_verification_setup.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/mo2f/mo2f_tfa/setup-two-factor/user/%user/tfa/otp-sms'] = array(
    'title' => 'OTP SMS',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tfa_user_otp_sms'),
    'access callback' => 'tfa_permission_for_authentic_user',
    'access arguments' => array(1),
    'file' => 'setup_two_factor.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/mo2f/mo2f_tfa/settings'] = array(
    'title' => 'Login Settings',
    'description' => 'Configuration setting for miniorange 2 factor authentication',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tfa_user_settings'),
    'access arguments' => array('administer tfa login'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  $items['admin/config/mo2f/mo2f_tfa/advanced_settings'] = array(
    'title' => 'Advanced Options',
    'description' => 'Configuration setting for miniorange 2 factor authentication',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tfa_user_advanced_settings'),
    'access arguments' => array('administer tfa login'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );

  $items['admin/config/mo2f/mo2f_tfa/troubleshooting'] = array(
    'title' => 'Help & Troubleshooting',
    'page callback' => 'tfa_user_help_and_troubleshooting',
    'access callback' => 'tfa_permission_for_authentic_user',
    'access arguments' => array('access content'),
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  $items['user/%user/nojs/tfa/resend-otp'] = array(
    'title' => 'Configure mobile',
    'page callback' => 'tfa_user_send_otp',
    'page arguments' => array(1, 2),
    // Access not required as this is for anonymous user.
    'access callback' => TRUE,
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['user/%user/ajax/tfa/resend-otp'] = array(
    'delivery callback' => 'ajax_deliver',
  ) + $items['user/%user/nojs/tfa/resend-otp'];

  $items['user/%user/tfa/authenticate'] = array(
    'title' => 'User account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tfa_user_login_2_facter', 1),
    // Access not required as this is for anonymous user.
    'access callback' => TRUE,
    'file' => 'tfa_user_login.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/mo2f/mo2f_tfa/how-it-works'] = array(
    'title' => 'How It Works',
    'page callback' => 'tfa_user_how_it_works',
    'access callback' => 'tfa_permission_for_authentic_user',
    'access arguments' => array('access content'),
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );

  $items['admin/config/mo2f/mo2f_tfa/qrtest'] = array(
    'title' => 'Test QR Code Authentication',
    'page callback' => 'tfa_user_qrtest',
    'access callback' => 'tfa_permission_for_authentic_user',
    'access arguments' => array('access content'),
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_CALLBACK,
    'weight' => 4,
    'options' => array('query' => array('qrcode' => 'test')),
  );

  $items['admin/config/mo2f/mo2f_tfa/gatest'] = array(
    'title' => 'Test Google Authenticator',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tfa_user_gatest'),
    'access callback' => 'tfa_permission_for_authentic_user',
    'access arguments' => array('access content'),
    'file' => 'tfa_login_customer_setup.inc',
    'type' => MENU_CALLBACK,
    'weight' => 5,
  );

/*  $items['user/%user/miniorange-settings'] = array(
    'title' => 'Configure miniOrange',
    'page callback' => '_redirect_user_miniorange',
    'access callback' => 'tfa_permission_for_authentic_user',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );*/

  return $items;
}

/**
 * Redirect user from user view and edit page.
 */
function _redirect_user_miniorange() {
  drupal_goto('admin/config/mo2f/mo2f_tfa');
  exit;
}

/**
 * Access callback function for 2 facter setup in authentic user.
 */
function tfa_permission_for_authentic_user() {
  global $user;
  if (user_is_logged_in() && (variable_get('two_factor_authentication_mo2f_disabled_status') || in_array('authenticated user', $user->roles))) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Implements hook_permission().
 */
function two_factor_authentication_permission() {
  return array(
    'administer tfa login' => array(
      'title' => t('Administer two Factor login'),
      'description' => t('Perform administration tasks for TFA login.'),
    ),
    'configure two factor' => array(
      'title' => t('Configure two factor'),
      'description' => t('Permission to perform two factor setup'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function two_factor_authentication_theme() {
  return array(
    'tfa_troubleshooting' => array('template' => 'tfa_login_troubleshooting',
    ),
    'tfa_mobile_config' => array('template' => 'tfa_mobile_config',
    ),
    'tfa_howitworks' => array('template' => 'tfa_how_it_works',
    ),
  );
}

/**
 * Setting form to change the miniorange connection settings.
 */
function tfa_user_settings($form, &$form_state) {
  global $user;
  $current_user = user_load($user->uid);
  if (isset($current_user->field_configured_methods[LANGUAGE_NONE]) && !empty($current_user->field_configured_methods[LANGUAGE_NONE])) {
    $disable_settings = FALSE;
  }
  else {
    $disable_settings = TRUE;
    drupal_set_message(t('Please <a href="@clicklink">Register with miniOrange</a> to configure miniOrange 2 Factor plugin.', array(
      '@clicklink' => url('admin/config/mo2f/mo2f_tfa', array('absolute' => TRUE)),
    ), array('html' => TRUE)), 'error');
  }

  $form['context1']['#markup'] = t('<h3>Select Roles to enable 2-Factor</h3>');

  $form['two_factor_authentication_mo2f_admin_disabled_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable 2-Factor for admins.'),
    '#description' => t('Note: This option is checked by default. It will enable 2-Factor only for admins, other users can still login with their password.'),
    '#default_value' => variable_get('two_factor_authentication_mo2f_admin_disabled_status'),
    '#disabled' => $disable_settings,
  );

/*  $form['two_factor_authentication_mo2f_disabled_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable 2-Factor for all other users.'),
    '#description' => t('Note: Checking this option will enable 2-Factor for all users. Make sure you have tested login with 2-factor before enabling it for all users.'),
    '#default_value' => variable_get('two_factor_authentication_mo2f_disabled_status'),
    '#disabled' => $disable_settings,
  );*/

  $form['context2']['#markup'] = t('<h3>Select Login Screen Options</h3>');

  $options = array(
    'passsword_plus_2factor' => ' Login with password + 2nd Factor <span class="textcolorred">(Recommended)</span>',
//    '2factor_only' => 'Login with 2nd Factor only <span class="textcolorred">(No password required.)</span>',
  );

  $two_factor_authentication_mo2f_login_option = variable_get('two_factor_authentication_mo2f_login_option', 'passsword_plus_2factor');
  $form['two_factor_authentication_mo2f_login_option'] = array(
    '#type' => 'radios',
    '#description' => t('Note: This option is checked by default so that if anybody has forgotten his phone or phone is lost/stolen/discharged. An OTP over registered email will be send to verify the user. User has to enter OTP to bypass login with your phone.'),
    '#default_value' => $two_factor_authentication_mo2f_login_option,
    '#options' => $options,
    '#ajax' => array(
      'callback' => 'ajax_login_option_callback',
      'wrapper' => 'replace_checkbox_div',
    ),
    '#disabled' => $disable_settings,
  );

  $form['replace_checkbox'] = array(
    '#type' => 'fieldset',
    '#title' => t("Settings"),
    '#prefix' => '<div id="replace_checkbox_div">',
    '#suffix' => '</div>',
  );

  if ((isset($form_state['values']) && $form_state['values']['two_factor_authentication_mo2f_login_option'] == 'passsword_plus_2factor') || (empty($form_state['values']) && $two_factor_authentication_mo2f_login_option == 'passsword_plus_2factor')) {
    $form['replace_checkbox']['mo2f_remember_device_test'] = array(
      '#type' => 'item',
      '#title' => t("Remember device Coming Soon"),
    );
  }
  else {
    $form['replace_checkbox']['two_factor_authentication_mo2f_hide_default_login'] = array(
      '#type' => 'checkbox',
      '#title' => t("I want to hide default login form."),
      '#description' => t("Note: Checking this option will display an option 'Remember this device' on 2nd factor screen. In the next login from the same device, user will bypass 2nd factor, i.e. user will be logged in through username + password only."),
      '#default_value' => variable_get('two_factor_authentication_mo2f_hide_default_login'),
    );
  }

  $form['context3']['#markup'] = t('<h3>What happens if my phone is lost, discharged or not with me</h3>');

  $form['two_factor_authentication_mo2f_enable_forgotphone'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Forgot Phone.<span class="textcolorred">( If you disable this checkbox, then users will not get this option.)</span>'),
    '#description' => t('Note: This option is checked by default so that if anybody has forgotten his phone or phone is lost/stolen/discharged. An OTP over registered email will be send to verify the user. User has to enter OTP to bypass login with your phone.'),
    '#default_value' => variable_get('two_factor_authentication_mo2f_enable_forgotphone'),
    '#disabled' => $disable_settings,
  );

  $form['context4']['#markup'] = t('<h3>Enable Two-Factor plugin</h3>');

  $form['two_factor_authentication_mo2f_show_loginwith_phone'] = array(
    '#type' => 'checkbox',
    '#title' =>
    t('Enable Two-Factor plugin.') . '<span style="color:red;">' . t('<span class="textcolorred">(If you disable this checkbox, Two-Factor plugin will not invoke for any user during login.)</span>') . '</span>',
    '#description' => t('Note: Disabling this option will allow all users to login with their username and password. Two-Factor will not invoke during login.'),
    '#default_value' => variable_get('two_factor_authentication_mo2f_show_loginwith_phone'),
    '#disabled' => $disable_settings,
  );

  $form['#submit'] = array('tfa_user_settings_form_submit');

  return system_settings_form($form);
}

/**
 * Helper function for ajax option.
 */
function ajax_login_option_callback($form, $form_state) {
  return $form['replace_checkbox'];
}

/**
 * Form submit handler for user settings.
 */
function tfa_user_settings_form_submit($form, &$form_state) {
  variable_set('two_factor_authentication_mo2f_login_option', $form_state['values']['two_factor_authentication_mo2f_login_option']);
  $clear_cache = FALSE;
/*  if (($form_state['values']['two_factor_authentication_mo2f_login_option'] == '2factor_only') && (empty($form_state['values']['mo2f_remember_device']))) {
    if (isset($form_state['values']['mo2f_remember_device'])) {
      variable_set('mo2f_remember_device', $form_state['values']['mo2f_remember_device']);
    }
    variable_del('two_factor_authentication_mo2f_hide_default_login');
    $clear_cache = TRUE;
  }*/
  if (($form_state['values']['two_factor_authentication_mo2f_login_option'] == 'passsword_plus_2factor') && (empty($form_state['values']['two_factor_authentication_mo2f_hide_default_login']))) {
    if (isset($form_state['values']['two_factor_authentication_mo2f_hide_default_login'])) {
      variable_set('two_factor_authentication_mo2f_hide_default_login', $form_state['values']['two_factor_authentication_mo2f_hide_default_login']);
    }
    variable_del('mo2f_remember_device');
    $clear_cache = TRUE;
  }

  if ($clear_cache) {
    cache_clear_all();
  }
}

/**
 * Setting form to change the miniorange connection settings.
 */
function tfa_user_advanced_settings($form, &$form_state) {
  $my_field = field_info_field('field_active_methods');
  $allowed_values = list_allowed_values($my_field);

  $options = $allowed_values;
  $selected_method = explode('|', variable_get('mp2f_active_methods'));
  if (empty($selected_method)) {
    $selected_method = array_flip($options);
  }
  $form['context5']['#markup'] = t('<h3>Select Two-Factor Methods for Users</h3>');
  $form['active'] = array(
    '#type' => 'checkboxes',
    '#description' => t('Note: You can select which Two Factor methods you want to enable for your users. By default all Two Factor methods are enabled for all users.'),
    '#default_value' => $selected_method,
    '#options' => $options,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Settings',
  );

  return $form;
}

/**
 * Form submit handler for advanced form settings page.
 */
function tfa_user_advanced_settings_submit($form, &$form_state) {
  $selected_method = array_flip($form_state['values']['active']);
  unset($selected_method[0]);
  $selected_method_string = implode('|', $selected_method);
  variable_set('mp2f_active_methods', $selected_method_string);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function two_factor_authentication_form_user_login_block_alter(&$form, &$form_state, $form_id) {
  // Alter the drupal login form blocks.
  _two_factor_authentication_user_login_form_alter($form, $form_state, $form_id);
}

/**
 * Implements hook_block_info_alter().
 */
function two_factor_authentication_block_info_alter(&$blocks, $theme, $code_blocks) {
  $specific_pages_value = array();
  $tfa_user_login_exclude = TFA_FACTOR_AUTHENTICATION_LOGIN_EXCLUDE;
  $specific_pages_value = explode(PHP_EOL, $blocks['user']['login']['pages']);
  if (!in_array($tfa_user_login_exclude, $specific_pages_value)) {
    array_push($specific_pages_value, $tfa_user_login_exclude);
    $final_appended_value = implode(PHP_EOL, $specific_pages_value);
    $blocks['user']['login']['pages'] = $final_appended_value;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function two_factor_authentication_form_user_login_alter(&$form, &$form_state, $form_id) {
  // Alter the drupal login form page.
  _two_factor_authentication_user_login_form_alter($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function two_factor_authentication_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  if (!in_array('authenticated user', $user->roles)) {
    $form['field_miniorange_email']['#access'] = FALSE;
    $form['field_registration_status']['#access'] = FALSE;
  }
}

/**
 * Helper function to alter the drupal login form.
 */
function _two_factor_authentication_user_login_form_alter(&$form, &$form_state, $form_id) {
  $twofactor_plugin = variable_get('two_factor_authentication_mo2f_show_loginwith_phone');
  $twofactor_admin_status = variable_get('two_factor_authentication_mo2f_admin_disabled_status');
//  $twofactor_users_status = variable_get('two_factor_authentication_mo2f_disabled_status');
  if (!$twofactor_plugin) {
    return;
  }
  if (!$twofactor_admin_status) {
    if (isset($twofactor_users_status)) {
      return;
    }
  }
  $two_factor_authentication_mo2f_login_option = variable_get('two_factor_authentication_mo2f_login_option');
  $form_state['two_factor_authentication_mo2f_login_option'] = $two_factor_authentication_mo2f_login_option;
  if ($two_factor_authentication_mo2f_login_option == 'passsword_plus_2factor') {
    $form['#validate'] = array('two_factor_authentication_qr_code_validate');
    $form['#submit'] = array('two_factor_authentication_qr_code_submit');
  }
  /*elseif ($two_factor_authentication_mo2f_login_option == '2factor_only') {
    if (variable_get('two_factor_authentication_mo2f_hide_default_login')) {
      $form['name']['#access'] = FALSE;
      $form['pass']['#access'] = FALSE;
      $form['#validate'] = array('two_factor_authentication_qr_code_validate');
      $form['#submit'] = array('two_factor_authentication_qr_code_submit');

      $form['tfa_identifier'] = array(
        '#type' => 'textfield',
        '#title' => t('Log in using TFA'),
        '#size' => $form['name']['#size'],
        '#maxlength' => 255,
        '#weight' => -1,
      );
    }
    else {
      $form['#attached']['js'][] = drupal_get_path('module', 'two_factor_authentication') . '/js/tfa_login.js';
      $form['#attached']['library'][] = array('system', 'jquery.cookie');

      if (!empty($form_state['values']['tfa_identifier'])) {
        $form['name']['#required'] = FALSE;
        $form['pass']['#required'] = FALSE;
        $form['#validate'] = array('two_factor_authentication_qr_code_validate');
        $form['#submit'] = array('two_factor_authentication_qr_code_submit');
      }

      $items = array();
      $items[] = array(
        'data' => l(t('Log in using TFA'), '#openid-login', array(
          'external' => TRUE,
        )),
        'class' => array('tfa-link'),
      );
      $items[] = array(
        'data' => l(t('Cancel TFA login'), '#', array('external' => TRUE)),
        'class' => array('user-link'),
      );

      $form['tfa_links'] = array(
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => array('class' => array('tfa-links')),
        '#weight' => 1,
      );

      $form['links']['#weight'] = 2;

      $form['tfa_identifier'] = array(
        '#type' => 'textfield',
        '#title' => t('Log in using TFA'),
        '#size' => $form['name']['#size'],
        '#maxlength' => 255,
        '#weight' => -1,
      );
    }
  }*/
}

/**
 * Submit login form with username and redirect to the qr code page.
 */
function two_factor_authentication_qr_code_submit($form, &$form_state) {

  if (isset($form_state['user']->field_miniorange_email[LANGUAGE_NONE]) && !empty($form_state['user']->field_miniorange_email[LANGUAGE_NONE])) {
    $email = $form_state['user']->field_miniorange_email[LANGUAGE_NONE][0]['value'];
  }
  $username = $form_state['values']['name'];
  $password = $form_state['values']['pass'];
  unset($_GET['destination']);

  $factor_admin_disable = variable_get('two_factor_authentication_mo2f_admin_disabled_status');
//  $factor_users_disable = variable_get('two_factor_authentication_mo2f_disabled_status');

  if (isset($form_state['user']->field_active_methods) && !empty($form_state['user']->field_active_methods) && ($form_state['u_role'] == 'user' && !empty($factor_users_disable) || $form_state['u_role'] == 'admin' && !empty($factor_admin_disable))) {
    if ($form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
      test_email_verification($email, 'OUT OF BAND EMAIL');
      $form_state['redirect'] = 'user/' . $form_state['uid'] . '/tfa/authenticate';
    }
    elseif (user_authenticate($username, $password)) {
      if ($form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value'] != TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
        mo2f_login_verification($email);
      }
      $form_state['redirect'] = 'user/' . $form_state['uid'] . '/tfa/authenticate';
    }
    elseif (!empty($form_state['values']['tfa_identifier']) && !empty($form_state['user']->uid)) {
      // This is for QR code.
      if (!empty($form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value']) && $form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION) {
        // Set session values for QR code login.
        $email = $form_state['user']->field_miniorange_email[LANGUAGE_NONE][0]['value'];
        mo2f_login_verification($email);
        // Redirect to the QR code login page.
        $form_state['redirect'] = 'user/' . $form_state['user']->uid . '/tfa/authenticate';
      }
      elseif (!empty($form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value']) && $form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR) {
        // Google Authentication.
        $_SESSION['mo2f'] = 'google';
        $form_state['redirect'] = 'user/' . $form_state['user']->uid . '/tfa/authenticate';
      }
      else {
        drupal_set_message(t('You have not configured any login methods! Please contact system administrator.'), 'error');
      }
    }
    else {
      drupal_set_message(t('Please enter valid credentials'), 'error');
    }
  }
  else {
    if (user_authenticate($username, $password)) {
      if (!empty($form_state['2fa_redirect'])) {
        user_login_submit(array(), $form_state);
        $form_state['redirect'] = 'admin/config/mo2f/mo2f_tfa';
        drupal_set_message('A new security system has been enabled to better protect your account. Please configure your Two-Factor Authentication method by registering your account.');
      }
      else {
        user_login_submit(array(), $form_state);
      }
    }
    elseif (!empty($form_state['values']['tfa_identifier']) && !empty($form_state['user']->uid)) {
      if (!empty($form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value']) && $form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION) {
        // Set session values for QR code login.
        $email = $form_state['user']->field_miniorange_email[LANGUAGE_NONE][0]['value'];
        mo2f_login_verification($email);
        // Redirect to the QR code login page.
        $form_state['redirect'] = 'user/' . $form_state['user']->uid . '/tfa/authenticate';
      }
      elseif (!empty($form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value']) && $form_state['user']->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR) {
        // Google Authentication.
        $_SESSION['mo2f'] = 'google';
        $form_state['redirect'] = 'user/' . $form_state['user']->uid . '/tfa/authenticate';
      }
      else {
        drupal_set_message(t('You have not configured any login methods! Please contact system administrator.'), 'error');
      }
    }
    else {
      drupal_set_message(t('Please enter valid credentials'), 'error');
    }
  }
}

/**
 * Validate the username for login with qr code.
 */
function two_factor_authentication_qr_code_validate(&$form, &$form_state) {
  $username = (!empty($form_state['values']['name'])) ? $form_state['values']['name'] : (!empty($form_state['values']['tfa_identifier']) ? $form_state['values']['tfa_identifier'] : '');

  if (!empty($username)) {
    $user = user_load_by_name($username);
    $form_state['2fa_redirect'] = FALSE;

    $factor_enable = variable_get('two_factor_authentication_mo2f_show_loginwith_phone');
    $factor_admin_disable = variable_get('two_factor_authentication_mo2f_admin_disabled_status');
   // $factor_users_disable = variable_get('two_factor_authentication_mo2f_disabled_status');
    if (in_array('administrator', $user->roles)) {
      if (isset($factor_enable)) {
        if (isset($factor_admin_disable)) {
          if (!isset($user->field_registration_status['und'][0]['value'])) {
            drupal_set_message('Please configure the second factor login method');
            $form_state['2fa_redirect'] = TRUE;
          }
        }
        $form_state['uid'] = $user->uid;
        $form_state['user'] = $user;
      }
      $form_state['u_role'] = 'admin';
    }
    else {
      if (isset($factor_enable)) {
        if (!empty($factor_users_disable)) {
          if (!isset($user->field_registration_status['und'][0]['value'])) {
            $form_state['2fa_redirect'] = TRUE;
          }
        }
        $form_state['uid'] = $user->uid;
        $form_state['user'] = $user;
      }
      $form_state['u_role'] = 'user';
    }

    if (empty($user)) {
      form_set_error('name', t('Please enter a valid username'));
    }
  }
}

/**
 * Helper function to verify user using email address.
 */
function test_email_verification($user, $mo2f_second_factor) {
  $challenge_mobile = new Customer_Setup();
  $content = $challenge_mobile->send_otp_token($user, $mo2f_second_factor, variable_get('two_factor_authentication_mo2f_customerKey'), variable_get('two_factor_authentication_mo2f_api_key'));
  $response = json_decode($content, TRUE);
  if (json_last_error() == JSON_ERROR_NONE) {
    if ($response['status'] == 'SUCCESS') {
      $_SESSION['mo2f']['mo2f-login-transactionId'] = $response['txId'];
      $sec_email = strstr($user, '@', TRUE);
      $first = substr($sec_email, 0, 1);
      $last = substr($sec_email, -1);
      $after = substr($user, strpos($user, "@") + 1);
      $sec_email = $first . 'xxxxxx' . $last . '@' . $after;
      $_SESSION['mo2f-login-message'] = $mo2f_second_factor == 'OUT OF BAND EMAIL' ? 'A verification email is sent to<b> ' . $sec_email . '</b>. Please click on accept link to verify your email.' : 'An email has been sent to ' . MO2f_Utility::mo2f_get_hiden_email($user) . '. We are waiting for your approval.';
      $_SESSION['mo_2factor_login_status'] = $mo2f_second_factor == 'OUT OF BAND EMAIL' ? 'MO_2_FACTOR_CHALLENGE_PUSH_NOTIFICATIONS' : 'MO_2_FACTOR_CHALLENGE_OOB_EMAIL';
      if (!empty($_SESSION['mo2f-login-message'])) {
        drupal_set_message($_SESSION['mo2f-login-message'], 'status');
      }
    }
    elseif ($response['status'] == 'ERROR' || $response['status'] == 'FAILED') {
      unset($_SESSION['mo2f']['mo2f-login-transactionId']);
      drupal_set_message(t('An error occured while processing your request. Please Try again.'), 'error');
    }
  }
  else {
    unset($_SESSION['mo2f']['mo2f-login-transactionId']);
    drupal_set_message(t('An error occured while processing your request. Please Try again.'), 'error');
  }
}

/**
 * Helper function send OTP on mail.
 */
function mo2f_login_verification($email) {
  $challenge_mobile = new customer_setup();
  $content = $challenge_mobile->send_otp_token($email, 'MOBILE AUTHENTICATION', variable_get('two_factor_authentication_mo2f_customerKey'), variable_get('two_factor_authentication_mo2f_api_key'));

  $response = json_decode($content, TRUE);

  if (json_last_error() == JSON_ERROR_NONE) {
    // Generate Qr code.
    $_SESSION['mo2f']['mo2f-login-qrCode'] = !empty($response['qrCode']) ? $response['qrCode'] : '';
    $_SESSION['mo2f']['mo2f-login-transactionId'] = !empty($response['txId']) ? $response['txId'] : '';
    $_SESSION['mo2f']['mo_2factor_login_status'] = 'MO_2_FACTOR_CHALLENGE_MOBILE_AUTHENTICATION';
  }
  else {
    drupal_set_message(t('Invalid request for mail OTP'), 'error');
  }
}

/**
 * Helper function for configure methods variable.
 */
function configured_methods_variable($configured_methods = array()) {
  global $user;
  $user_fields = user_load($user->uid);
  if (isset($user_fields->field_configured_methods) && !empty($user_fields->field_configured_methods)) {
    $configured_methods = $user_fields->field_configured_methods[LANGUAGE_NONE];
  }
  foreach ($configured_methods as $key => $configured_array) {
    $configured_methods[$key] = $configured_array['value'];
  }
  return $configured_methods;
}

/**
 * Implements hook_block_info().
 */
function two_factor_authentication_block_info() {
  $blocks['contact_us'] = array(
    'info' => t('CONTACT US block'),
    'region' => 'content',
    'status' => 1,
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'pages' => "admin/config/mo2f/mo2f_tfa/troubleshooting\nadmin/config/mo2f/mo2f_tfa/settings\nadmin/config/mo2f/mo2f_tfa/how-it-works\nadmin/config/mo2f/mo2f_tfa\nadmin/config/mo2f/mo2f_tfa/advanced_settings",
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function two_factor_authentication_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'contact_us':
      $block['content'] = two_factor_authentication_contact_us();
      break;
  }
  return $block;
}

/**
 * Helper function for contact us block display.
 */
function two_factor_authentication_contact_us() {
  module_load_include('inc', 'two_factor_authentication', 'tfa_login_customer_setup');
  $output = drupal_get_form('tfa_user_contact_us');
  return $output;
}
