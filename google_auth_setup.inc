<?php

/**
 * @file
 * Functions for Google Authentication.
 */

/**
 * Menu callback for google auth form.
 */
function google_auth() {
  return drupal_get_form('google_authenticator');
}

/**
 * Form callback for google auth.
 */
function google_authenticator($form, &$form_state) {

  // Display page 2 if $form_state['storage']['page_two'] is set.
  if (isset($form_state['storage']['ga_page_two'])) {
    return ga_setup_page($form_state);
  }
  // Page 1 is displayed if $form_state['storage']['page_two'] is not set.
  $form['step_1']['#markup'] = t('<div class="ga_auth_hr_st1">Step-1: Select phone Type</div><div class="ga_auth_st2">Step-2: Set up Google Authenticator</div><div class="ga_auth_st3">Step-3: Verify and Save</div>');

  $form['select_phone'] = array(
    '#type' => 'radios',
    '#default_value' => isset($form_state['values']['select_phone']) ? $form_state['values']['select_phone'] : '',
    '#options' => array(
      1 => t('Android'),
      2 => t('iPhone'),
      3 => t('Blackberry'),
    ),
  );

  $form['next'] = array(
    '#type' => 'submit',
    '#value' => 'Next >>',
  );

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => '<< Back',
  );

  $form['back']['#type'] = 'submit';
  $form['back']['#value'] = t('Back');

  return $form;
}

/**
 * New function created to help make the code more manageable.
 */
function ga_setup_page(&$form_state) {

  global $user;
  $user = user_load($user->uid);

  $form['select_phone'] = array(
    '#type' => 'radios',
    '#default_value' => isset($form_state['values']['select_phone']) ? $form_state['values']['select_phone'] : '',
    '#options' => array(
      1 => t('Android'),
      2 => t('iPhone'),
      3 => t('Blackberry'),
    ),
    '#prefix' => '<div class="step-1 form-step"><div class="ga_auth_hr">Step-1: Select phone Type</div>',
  );

  $form['next'] = array(
    '#type' => 'submit',
    '#value' => 'Next >>',
  );

  $form['back']['#type'] = 'submit';
  $form['back']['#value'] = t('Back');
  $form['back']['#suffix'] = '</div>';

  $mo2f_google_auth = isset($_SESSION['mo2f_google_auth']) ? $_SESSION['mo2f_google_auth'] : NULL;
  $data = isset($_SESSION['mo2f_google_auth']) ? $mo2f_google_auth['ga_qrCode'] : NULL;
  $ga_secret = isset($_SESSION['mo2f_google_auth']) ? $mo2f_google_auth['ga_secret'] : NULL;

  $step2_html = '';
  $step2_html .= '<div class="step-2 form-step">
              <div class="ga_auth_hr">Step-2: Set up Google Authenticator</div>';
  if ($form_state['values']['select_phone'] == 1) {
    $step2_html .= '<div class="para">Install the Google Authenticator App for Android.</div>
              <ol>
                <li>On your phone,Go to Google Play Store.</li>
                <li>Search for <strong>Google Authenticator</strong>. <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Download from the Google Play Store and install the application.</a></li>
              </ol>
              <div class="para">Now open and configure Google Authenticator.</div>
              <ol>
                <li>In Google Authenticator, touch Menu and select "Set up account."</li>
                <li>Select "Scan a barcode". Use your phone\'s camera to scan this barcode.</li>
              </ol>';
  }
  elseif ($form_state['values']['select_phone'] == 2) {
    $step2_html .= '<div class="para">Install the Google Authenticator app for iPhone.</div>
              <ol>
                <li>On your iPhone, tap the App Store icon.</li>
                <li>Search for Google Authenticator. <a href="https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8">Download from the App Store and install it</a></li>
              </ol>
              <div class="para">Now open and configure Google Authenticator.</div>
              <ol>
                <li>In Google Authenticator, tap "+", and then "Scan Barcode."</li>
                <li>Use your phone\'s camera to scan this barcode.</li>
              </ol>';
  }
  elseif ($form_state['values']['select_phone'] == 3) {
    $step2_html .= '<div class="para">Install the Google Authenticator app for BlackBerry</div>
      <ol>
        <li>On your phone, open a web browser.Go to <strong>m.google.com/authenticator</strong>.</li>
        <li>Download and install the Google Authenticator application.</a></li>
      </ol>
      <div class="para">Now open and configure Google Authenticator.</div>
      <ol>
        <li>In Google Authenticator, select Manual key entry.</li>
        <li>In "Enter account name" type your full email address.</li>
        <li>In "Enter key" type your secret key:' .
          '<div class="scan-ga-secret">' . $ga_secret .
          '<div class="scan-ga-secret-text">Spaces don\'t matter.</div></div>' .
        '</li>
        <li>Choose Time-based type of key.</li>
        <li>Tap Save.</li>
      </ol></div>';
  }

  if ($form_state['values']['select_phone'] != 3) {
    $step2_html .= '<table class="mo2f_settings_table">
                <div id="displayGaCode" class="ga_configure"><img src="data:image/jpg;base64,' . $data . '" />
                </div>
              </table>';
  }

  $form['step_2']['#markup'] = $step2_html;

  if ($form_state['values']['select_phone'] != 3 && $form_state['values']['select_phone'] == 1) {
    $form['cannot_scan'] = array(
      '#title' => 'Can\'t scan the barcode?',
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $scan = '';
    $scan .= '<ol>
            <li>In Google Authenticator, touch Menu and select "Set up account."</li>
            <li>Select "Enter provided key"</li>
            <li>In "Enter account name" type your full email address.</li>
            <li>In "Enter your key" type your secret key:' .
              '<div class="scan-ga-secret">' . $ga_secret .
              '<div class="scan-ga-secret-text">Spaces don\'t matter.</div></div>' .
            '</li>
            <li>Key type: make sure "Time-based" is selected.</li>
            <li>Tap Add.</li>
            </ol>
          </div>';
    $form['cannot_scan']['scan']['#markup'] = $scan;
  }
  elseif ($form_state['values']['select_phone'] != 3 && $form_state['values']['select_phone'] == 2) {
    $form['cannot_scan'] = array(
      '#title' => 'Can\'t scan the barcode?',
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $scan = '';
    $scan .= '<ol>
            <li>In Google Authenticator, tap +.</li>
            <li>Key type: make sure "Time-based" is selected.</li>
            <li>In "Account" type your full email address.</li>
            <li>In "Key" type your secret key:' .
              '<div class="scan-ga-secret">' . $ga_secret .
              '<div class="scan-ga-secret-text">Spaces don\'t matter.</div></div>' .
            '</li>
            <li>Tap Add.</li>
            </ol>
          </div>';
    $form['cannot_scan']['scan']['#markup'] = $scan;
  }

  $step3_html = '';
  $step3_html .= '<div class="step-3 form-step">
    <div class="ga_auth_hr">Step-3: Verify and Save</div>
    <div class="ga_text"><p>Once you have scanned the barcode, enter the 6-digit verification code generated by the Authenticator app</p></div>';

  $form['step_3']['#markup'] = $step3_html;

  $form['ga_otp'] = array(
    '#type' => 'textfield',
    '#title' => 'Code:',
    '#attributes' => array('placeholder' => t('Enter OTP')),
  );

  $form['validate_ga'] = array(
    '#type' => 'submit',
    '#value' => 'Verify And Save',
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Form submit handler for google auth form.
 */
function google_authenticator_submit($form, &$form_state) {
  if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'back') {
    drupal_goto('admin/config/mo2f/mo2f_tfa/setup-two-factor');
    exit;
  }

  global $user;
  $user = user_load($user->uid);
  $email = $user->field_miniorange_email[LANGUAGE_NONE][0]['value'];
  $configured_methods = array();
  $configured_methods = configured_methods_variable();
  if (isset($form_state['clicked_button']) && $form_state['triggering_element']['#parents'][0] == 'next') {
    if (isset($form_state['values']['select_phone']) && !empty($form_state['values']['select_phone'])) {

      $form_state['storage']['ga_page_two'] = TRUE;
      $form_state['storage']['ga_page_one_values'] = $form_state['values'];
      $form_state["rebuild"] = TRUE;
      mo2f_get_ga_for_mobile($email);
      if (isset($_SESSION['mo2f']['mo2f_show_ga_qr_code'])) {

        $mo2f_google_auth = isset($_SESSION['mo2f_google_auth']) ? $_SESSION['mo2f_google_auth'] : NULL;
        $ga_secret = isset($_SESSION['mo2f_google_auth']) ? $mo2f_google_auth['ga_secret'] : NULL;

        $phone_type = $form_state['values']['select_phone'];
        $google_auth = new Miniorange_Rba_Attributes();
        $google_response = json_decode($google_auth->mo2f_google_auth_service($email, TRUE));
        if (json_last_error() == JSON_ERROR_NONE) {
          if ($google_response->status == 'SUCCESS') {
            $mo2f_google_auth = array();
            $mo2f_google_auth['ga_qrCode'] = $google_response->qrCodeData;
            $mo2f_google_auth['ga_secret'] = $google_response->secret;
            $mo2f_google_auth['ga_phone'] = $phone_type;
            $_SESSION['mo2f_google_auth'] = $mo2f_google_auth;
          }
          else {
            drupal_set_message(t('Error occurred while registering the user. Please try again.'), 'error');
          }
        }
        else {
          drupal_set_message(t('Error occurred while registering the user. Please try again.'), 'error');
        }
      }
    }
    else {
      drupal_set_message(t('Please select phone type'), 'error');
    }
  }
  elseif (isset($form_state['clicked_button']) && $form_state['triggering_element']['#parents'][0] == 'back') {
      drupal_goto('admin/config/mo2f/mo2f_tfa/setup-two-factor');
  }

  else {
    $otp_token = $_POST['ga_otp'];
    $mo2f_google_auth = isset($_SESSION['mo2f_google_auth']) ? $_SESSION['mo2f_google_auth'] : NULL;
    $ga_secret = $mo2f_google_auth != NULL ? $mo2f_google_auth['ga_secret'] : NULL;
    if (MO2f_Utility::mo2f_check_number_length($otp_token)) {
      $google_auth = new Miniorange_Rba_Attributes();
      $google_response = json_decode($google_auth->mo2f_validate_google_auth($email, $otp_token, $ga_secret), TRUE);

      if (json_last_error() == JSON_ERROR_NONE) {
        if ($google_response['status'] == 'SUCCESS') {

          // Update miniorange about the default set method.
          $enduser = new Two_Factor_Setup();
          $response = json_decode($enduser->mo2f_update_userinfo($email, 'GOOGLE AUTHENTICATOR', NULL, NULL, NULL), TRUE);

          if (json_last_error() == JSON_ERROR_NONE) {
            if ($response['status'] == 'SUCCESS') {

              unset($_SESSION['mo2f_google_auth']);
              $user->field_active_methods[LANGUAGE_NONE][0]['value'] = $_SESSION['mo2f']['active'];
              if (!in_array(TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR, $configured_methods)) {
                array_push($user->field_configured_methods[LANGUAGE_NONE], array('value' => TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR));
              }
              user_save($user);
              drupal_set_message(t('Google Authenticator has been set as your 2nd factor method <a href="@clicklink">Click Here</a> to test Google Authenticator method.', array('@clicklink' => url('admin/config/mo2f/mo2f_tfa/gatest', array('absolute' => TRUE)))));
              drupal_goto('admin/config/mo2f/mo2f_tfa/setup-two-factor');
            }
            else {
              drupal_set_message(t('An error occured while processing your request. Please Try again.'), 'error');
            }
          }
          else {
            drupal_set_message(t('An error occured while processing your request. Please Try again.'), 'error');
          }
        }
        else {
          drupal_set_message(t('Error occurred while validating the OTP. Please try again. Possible causes: <br />1. You have enter invalid OTP.<br />2. You App Time is not sync.Go to settings and tap on Time correction for codes and tap on Sync now .'), 'error');
        }
      }
      else {
        drupal_set_message(t('Error occurred while validating the user. Please try again.'), 'error');
      }
    }
    else {
      drupal_set_message(t('Only digits are allowed. Please enter again.'), 'error');
    }
  }
}

/**
 * Function to get QR code.
 */
function mo2f_get_ga_for_mobile($email) {

  $register_mobile = new two_factor_setup();
  $content = $register_mobile->register_mobile($email);
  $response = json_decode($content, TRUE);

  if (json_last_error() == JSON_ERROR_NONE) {
    if (isset($response['statusCode']) && $response['statusCode'] == 'ERROR') {
      unset($_SESSION['mo2f']);
    }
    else {
      $_SESSION['mo2f']['mo2f_ga_qrCode'] = $response['qrCode'];
      $_SESSION['mo2f']['mo2f_transactionId'] = $response['txId'];
      $_SESSION['mo2f']['mo2f_show_ga_qr_code'] = 'MO_2_FACTOR_SHOW_QR_CODE_CONFIGURE';
    }
  }
}
