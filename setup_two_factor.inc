<?php

/**
 * @file
 * Two Factor Authentication form and functions for admin.
 */

/**
 * Implements hook_form().
 */
function two_factor_authentication_form($form, $form_state) {

  global $user, $base_url;
  $user = user_load($user->uid);
  $config = $register = '';
  $users_active_method = '';
  $users_active_method_name = '';
  $configured_methods = array();
  $configured_methods = configured_methods_variable();
  if (isset($user->field_active_methods) && !empty($user->field_active_methods)) {
    if ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
      $users_active_method_name = t('Email Verification');
      $users_active_method = TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION;
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_OTP_OVER_SMS) {
      $users_active_method_name = t('OTP Over SMS');
      $users_active_method = TWO_FACTOR_AUTHENTICATION_OTP_OVER_SMS;
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_PHONE_VERIFICATION) {
      $users_active_method_name = t('Phone Call Verification');
      $users_active_method = TWO_FACTOR_AUTHENTICATION_PHONE_VERIFICATION;
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN) {
      $users_active_method_name = t('Soft Token');
      $users_active_method = TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN;
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION) {
      $users_active_method_name = t('QR Code Authentication');
      $users_active_method = TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION;
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION) {
      $users_active_method_name = t('Push Notification');
      $users_active_method = TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION;
    }
    elseif ($user->field_active_methods[LANGUAGE_NONE][0]['value'] == TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR) {
      $users_active_method_name = t('Google Authenticator');
      $users_active_method = TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR;
    }
    else {
      $users_active_method_name = t('NONE');
      $users_active_method = $user->field_active_methods[LANGUAGE_NONE][0]['value'];
    }
  }
  else {
    $users_active_method_name = $users_active_method = 'NONE';
  }

  $form['header_top']['#markup']
    = '<div class="tfa-top">
      ' . $register . '
      <div class="tfa-top-left"><h3>Setup Two-Factor</h3></div>
      <div class="tfa-top-center-activated"> Active Method - ' . drupal_strtoupper($users_active_method_name) . '</div>
      <div class="tfa-top-right">' . l(t('Need Support?'), 'admin/config/mo2f/mo2f_tfa') . '</div>
    </div>
    <div class="tfa-info">Select any Two-Factor of your choice below and complete its setup. <a href="how-it-works">Click here to see How To Setup ?</a></div>';
    $form['header_methods']['#markup']
    = '<div class="header-methods-wrap">
        <div class="header-methods"><span class="methods-square am"></span>Active Method</div>
        <div class="header-methods"><span class="methods-square cm"></span>Configured Method</div>
        <div class="header-methods"><span class="methods-square um"></span>Unconfigured Method</div>
      </div>';

  $setup_text = '<div class="setup-text"><a href="how-it-works">How To Setup ?</a></div>';
  $desk_n_smart = '<div class="device-support desk-n-smart"></div>';
  $mobile_n_smart = '<div class="device-support mobile-n-smart"></div>';
  $all_device = '<div class="device-support all-device"></div>';
  $smart_only = '<div class="device-support smart-only"></div>';

  // Email verification.
  $email_verification_fieldset = t('<div class="method">Email Verification</div><div class="method-description">You will receive an email with link. You have to click the ACCEPT or DENY link to verify your email. Supported in Desktops, Laptops, Smartphones.</div>');

  if (in_array(TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION, $configured_methods)) {
    $config = 'config';
  }
  else {
    $config = 'unconfig';
  }
  if ($users_active_method == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
    $active = 'active';
  }
  else {
    $active = 'inactive';
  }

  if (isset($users_active_method) && $users_active_method == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION || in_array(TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION, $configured_methods)) {
    $email_verification_fieldset .= '<div class="' . $config . ' ' . $active . ' rt">' . $desk_n_smart . '<div class="rt-text">' . l(t('Test'), $base_url . '/admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/email-verification') . '</div></div>';
  }
  else {
    $email_verification_fieldset .= '<div class="' . $config . ' setup">' . $desk_n_smart . $setup_text . '</div>';
  }

  // Otp over sms.
  $otp_over_sms_fieldset = t('<div class="method">OTP Over SMS</div><div class="method-description">You will receive a one time passcode via SMS on your phone. You have to enter the otp on your screen to login. Supported in Smartphones, Feature Phones.</div>');

  if (isset($users_active_method) && $users_active_method == TWO_FACTOR_AUTHENTICATION_OTP_OVER_SMS) {
    $otp_over_sms_fieldset .= '<div class="rt">' . $mobile_n_smart . '<div class="rt-text"><a href="#">Reconfigure</a> | <a href="#">Test</a></div></div>';
  }
  else {
    $otp_over_sms_fieldset .= '<div class="setup">' . $mobile_n_smart . $setup_text . '</div>';
  }

  // Phone verification.
  $phone_verification_fieldset = t('<div class="method">Phone Call Verification</div><div class="method-description">You will receive a phone call telling a one time passcode. You have to enter the one time passcode to login. Supported in Landlines, Smartphones, Feature phones.</div>');

  if (isset($users_active_method) && $users_active_method == TWO_FACTOR_AUTHENTICATION_PHONE_VERIFICATION) {
    $phone_verification_fieldset .= '<div class="rt">' . $all_device . '<div class="rt-text"><a href="#">Reconfigure</a> | <a href="#">Test</a></div></div>';
  }
  else {
    $phone_verification_fieldset .= '<div class="setup">' . $all_device . $setup_text . '</div>';
  }

  // Soft token.
  $soft_token_fieldset = t('<div class="method">Soft Token</div><div class="method-description">You have to enter 6 digits code generated by miniOrange Authenticator App like Google Authenticator code to login. Supported in Smartphones only.</div>');

  if (in_array(TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN, $configured_methods)) {
    $config = 'config';
  }
  else {
    $config = 'unconfig';
  }
  if ($users_active_method == TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN) {
    $active = 'active';
  }
  else {
    $active = 'inactive';
  }

  if (isset($users_active_method) && $users_active_method == TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN || in_array(TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN, $configured_methods)) {
    $soft_token_fieldset .= '<div class="' . $config . ' ' . $active . ' rt">' . $smart_only . '<div class="rt-text">' . l(t('Reconfigure'), $base_url . '/admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/configure-mobile') . ' | <a href="#">Test</a></div></div>';
  }
  else {
    $soft_token_fieldset .= '<div class="' . $config . ' setup">' . $smart_only . $setup_text . '</div>';
  }

  // Qr code authentication.
  $qr_code_authentication_fieldset = t('<div class="method">QR Code Authentication</div><div class="method-description">You have to scan the QR Code from your phone using miniOrange Authenticator App to login. Supported in Smartphones only.</div>');

  if (in_array(TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION, $configured_methods)) {
    $config = 'config';
  }
  else {
    $config = 'unconfig';
  }
  if ($users_active_method == TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION) {
    $active = 'active';
  }
  else {
    $active = 'inactive';
  }

  if (isset($users_active_method) && $users_active_method == TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION || in_array(TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION, $configured_methods)) {
    $qr_code_authentication_fieldset .= '<div class="' . $config . ' ' . $active . ' rt">' . $smart_only . '<div class="rt-text">' . l(t('Reconfigure'), $base_url . '/admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/configure-mobile') . ' | <a href="' . url("admin/config/mo2f/mo2f_tfa/qrtest", array("absolute" => TRUE, 'query' => array('qrcode' => 'test'))) . '">' . t('Test') . '</a></div></div>';
  }
  else {
    $qr_code_authentication_fieldset .= '<div class="' . $config . ' setup">' . $smart_only . $setup_text . '</div>';
  }

  // Push notification.
  $push_notification_fieldset = t('<div class="method">Push Notification</div><div class="method-description">You will receive a push notification on your phone. You have to ACCEPT or DENY it to login. Supported in Smartphones only.</div>');

  if (in_array(TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION, $configured_methods)) {
    $config = 'config';
  }
  else {
    $config = 'unconfig';
  }
  if ($users_active_method == TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION) {
    $active = 'active';
  }
  else {
    $active = 'inactive';
  }

  if (isset($users_active_method) && $users_active_method == TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION || in_array(TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION, $configured_methods)) {
    $push_notification_fieldset .= '<div class="' . $config . ' ' . $active . ' rt">' . $smart_only . '<div class="rt-text">' . l(t('Reconfigure'), $base_url . '/admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/configure-mobile') . ' | <a href="#">Test</a></div></div>';
  }
  else {
    $push_notification_fieldset .= '<div class="setup">' . $smart_only . $setup_text . '</div>';
  }

  // Google authenticator.
  $google_authenticator_fieldset = t('<div class="method">Google Authenticator</div><div class="method-description">You have to enter 6 digits code generated by Google Authenticaor App to login. Supported in Smartphones<br/> only.</div>');

  if (in_array(TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR, $configured_methods)) {
    $config = 'config';
  }
  else {
    $config = 'unconfig';
  }
  if ($users_active_method == TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR) {
    $active = 'active';
  }
  else {
    $active = 'inactive';
  }

  if (isset($users_active_method) && $users_active_method == TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR || in_array(TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR, $configured_methods)) {
    $google_authenticator_fieldset .= '<div class="' . $config . ' ' . $active . ' rt">' . $smart_only . '<div class="rt-text">' . l(t('Reconfigure'), $base_url . '/admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/google-auth') . ' | <a href="' . url("admin/config/mo2f/mo2f_tfa/gatest", array("absolute" => TRUE)) . '">Test</a></div></div>';
  }
  else {
    $google_authenticator_fieldset .= '<div class="' . $config . ' setup">' . $smart_only . $setup_text . '</div>';
  }

  if (empty($user->field_registration_status[LANGUAGE_NONE][0]['value']) || !in_array(
        $user->field_registration_status[LANGUAGE_NONE][0]['value'], array(
          TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_INITIALIZE_MOBILE_REGISTRATION,
          TWO_FACTOR_AUTHENTICATION_MO_2_FACTOR_PLUGIN_SETTINGS,
        )
    )) {
    $radio_disabled = TRUE;

    $register = drupal_set_message(t('Please <a href="@register">Register with miniOrange</a> to configure miniOrange 2 Factor plugin.', array('@register' => url('admin/config/mo2f/mo2f_tfa', array('absolute' => TRUE)))), 'warning');

  }
  else {
    $radio_disabled = FALSE;
  }

  $options = array(
    TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION => $email_verification_fieldset,
    //TWO_FACTOR_AUTHENTICATION_OTP_OVER_SMS => $otp_over_sms_fieldset,
    //TWO_FACTOR_AUTHENTICATION_PHONE_VERIFICATION => $phone_verification_fieldset,
    //TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN => $soft_token_fieldset,
    TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION => $qr_code_authentication_fieldset,
    //TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION => $push_notification_fieldset,
    TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR => $google_authenticator_fieldset,
  );

  if (user_is_logged_in() && !in_array('authenticated user', $user->roles)) {
    $option_enabled = array_flip(explode('|', variable_get('mp2f_active_methods')));
    $options = array_intersect_key($options, $option_enabled);
  }

  $selected_method = (isset($users_active_method)) ? $users_active_method : '';

  $form['active'] = array(
    '#type' => 'radios',
    '#default_value' => $selected_method,
    '#options' => $options,
    '#disabled' => $radio_disabled,
  );

  $form['active'][$selected_method]['#attributes']['class'][] = 'activated-method';

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'submit',
  );
  return $form;
}

/**
 * Helper function to add sms field to form.
 */
function tfa_user_otp_sms($form, &$form_state) {

  $form['otp_sms'] = array(
    '#type' => 'textfield',
    '#title' => 'Enter One Time passcode',
    '#description' => t('Enter the phone number'),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function two_factor_authentication_form_submit($form, &$form_state) {

  global $user;
  $user_fields = user_load($user->uid);
  $email = $user_fields->field_miniorange_email[LANGUAGE_NONE][0]['value'];
  $configured_methods = array();
  $configured_methods = configured_methods_variable();
  $_SESSION['mo2f']['active'] = $form_state['values']['active'];
  $enduser = new Two_Factor_Setup();

  if ($form_state['values']['active'] == TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION) {
    if (in_array(TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION, $configured_methods)) {
      $form_state['rebuild'] = TRUE;
      $user_fields->field_active_methods[LANGUAGE_NONE][0]['value'] = array(TWO_FACTOR_AUTHENTICATION_EMAIL_VERIFICATION);
      user_save($user_fields);
      drupal_set_message(t('Email Verification is set as your Two-Factor method.'), 'status');

      // Update miniorange about the default set method.
      if(!empty($email))
        $enduser->mo2f_update_userinfo($email, 'OUT OF BAND EMAIL', NULL, NULL, NULL);
    }
    else {
      $form_state['redirect'] = 'admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/email-verification';
    }
  }
  elseif ($form_state['values']['active'] == TWO_FACTOR_AUTHENTICATION_OTP_OVER_SMS) {
  }
  elseif ($form_state['values']['active'] == TWO_FACTOR_AUTHENTICATION_PHONE_VERIFICATION) {
  }
  elseif ($form_state['values']['active'] == TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN) {
    if (in_array(TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN, $configured_methods)) {
      $form_state['rebuild'] = TRUE;
      $user_fields->field_active_methods[LANGUAGE_NONE][0]['value'] = array(TWO_FACTOR_AUTHENTICATION_SOFT_TOKEN);
      user_save($user_fields);
      drupal_set_message(t('Soft Token is set as your Two-Factor method.'), 'status');
    }
    else {
      $form_state['redirect'] = 'admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/configure-mobile';
    }
  }
  elseif ($form_state['values']['active'] == TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION) {
    if (in_array(TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION, $configured_methods)) {
      $form_state['rebuild'] = TRUE;
      $user_fields->field_active_methods[LANGUAGE_NONE][0]['value'] = array(TWO_FACTOR_AUTHENTICATION_QR_CODE_AUTHENTICATION);
      user_save($user_fields);
      drupal_set_message(t('QR Code Authentication is set as your Two-Factor method.'), 'status');

      // Update miniorange about the default set method.
      if(!empty($email))
        $enduser->mo2f_update_userinfo($email, 'MOBILE AUTHENTICATION', NULL, NULL, NULL);
    }
    else {
      $form_state['redirect'] = 'admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/configure-mobile';
    }
  }
  elseif ($form_state['values']['active'] == TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION) {
    if (in_array(TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION, $configured_methods)) {
      $form_state['rebuild'] = TRUE;
      $user_fields->field_active_methods[LANGUAGE_NONE][0]['value'] = array(TWO_FACTOR_AUTHENTICATION_PUSH_NOTIFICATION);
      user_save($user_fields);
      drupal_set_message(t('Push Notification is set as your Two-Factor method.'), 'status');
    }
    else {
      $form_state['redirect'] = 'admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/configure-mobile';
    }
  }
  elseif ($form_state['values']['active'] == TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR) {
    if (in_array(TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR, $configured_methods)) {
      $form_state['rebuild'] = TRUE;
      $user_fields->field_active_methods[LANGUAGE_NONE][0]['value'] = array(TWO_FACTOR_AUTHENTICATION_GOOGLE_AUTHENTICATOR);
      user_save($user_fields);
      drupal_set_message(t('Google Authenticator is set as your Two-Factor method.'), 'status');

      // Update miniorange about the default set method.
      if(!empty($email))
        $enduser->mo2f_update_userinfo($email, 'GOOGLE AUTHENTICATOR', NULL, NULL, NULL);
    }
    else {
      $form_state['redirect'] = 'admin/config/mo2f/mo2f_tfa/setup-two-factor/user/' . $user->uid . '/tfa/google-auth';
    }
  }
}
