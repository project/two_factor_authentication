<?php
/**
 * @file
 * Tpl file for mobile config page.
 */
?>
<table>
  <h3>
    Download the miniOrange <span style="color: #F78701;">i'm me</span>
    app
  </h3>
  <hr>
  <tr>
    <div class="panel-body">
      <td style="width: 55%;">
        <p class="content_fonts" style="margin-bottom: 2px !important;">
          <b>iPhone Users</b>
        </p>
        <ol>
          <li>Go to App Store</li>
          <li>Search for <b>miniOrange</b></li>
          <li>Download and install <span style="color: #F78701;"><b>miniOrange
                Authenticator</b></span> app (<b>NOT MOAuth</b>)
          </li>
        </ol> <span><a target="_blank" href="https://itunes.apple.com/us/app/miniorange-authenticator/id796303566?ls=1"><img src="<?php print base_path() . drupal_get_path('module', 'two_factor_authentication') . '/images/appstore.png'; ?>" style="width: 120px; height: 45px; margin-left: 6px;"></a></span><br>
        <br>
      </td>
      <td>
        <p class="content_fonts"
          style="margin-bottom: 2px !important; margin-top: -10px;">
          <b>Android Users</b>
        </p>
        <ol>
          <li>Go to Google Play Store.</li>
          <li>Search for <b>miniOrange.</b></li>
          <li>Download and install miniOrange <span
            style="color: #F78701;"><b>miniOrange Authenticator</b></span> app (<b>NOT MOAuth</b>)
          </li>
        </ol> <a target="_blank"
        href="https://play.google.com/store/apps/details?id=com.miniorange.authbeta"><img
          src="<?php print base_path() . drupal_get_path('module', 'two_factor_authentication') . '/images/playStore.png'; ?>"
          style="width: 120px; height: =45px; margin-left: 6px;"></a>
      </td>
    </div>
  </tr>
</table>
