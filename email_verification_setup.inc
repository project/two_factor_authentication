<?php

/**
 * @file
 * Email verification functions.
 */

/**
 * Menu callback for email verification.
 */
function email_verification() {
  return drupal_get_form('email_verification_set');
}

/**
 * Sets session data for the requested user.
 */
function email_verification_set($form, &$form_state) {

  global $base_url, $user;
  $user = user_load($user->uid);
  $email = $user->field_miniorange_email[LANGUAGE_NONE][0]['value'];
  mo2f_login_verification($email);
  test_email_verification($email, 'OUT OF BAND EMAIL');
  $path = drupal_get_path('module', 'two_factor_authentication');
  $url = variable_get('two_factor_authentication_mo2f_host_name');
  $form['#attached']['js'][] = array(
    'data' => array(
      'two_factor_authentication' => array(
        'mo2f_qrCode' => $_SESSION['mo2f']['mo2f-login-transactionId'],
        'path' => $path,
        'url' => $url,
      ),
    ),
    'type' => 'setting',
  );
  $form['header']['#markup'] = t('<h4>Test Email Verification</h4><div class="email-wrap email-verification"><h6>A verification email is sent to your registered email.<br> We are waiting for your approval...</h6>');
  $image_path = file_create_url($base_url . '/' . drupal_get_path('module', 'two_factor_authentication') . '/images/ajax-loader-login.gif');
  $form['loader']['#markup'] = '<div><img src="' . $image_path . '"></div>';
  $form['back_to_setup'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#suffix' => '</div>',
  );

  $form['test_email_verify_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test Email Submit'),
    '#attributes' => array('class' => array('element-invisible')),
  );

  $form['#action'] = 'admin/config/mo2f/mo2f_tfa/setup-two-factor';

  return $form;
}

/**
 * Form submit handler for email verify.
 */
function email_verification_set_submit($form, &$form_state) {

  if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'back_to_setup') {
    unset($_SESSION['mo2f']);
  }
  elseif (isset($form_state['triggering_element']) && $form_state['triggering_element']['#parents'][0] == 'test_email_verify_submit') {
    drupal_set_message(t('You have successfully completed the test.'), 'status');
    unset($_SESSION['mo2f']);
    unset($_SESSION['mo2f-login-message']);
  }
}
